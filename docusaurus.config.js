module.exports = {
    plugins: [],
    title: 'Douglas Caina',
    tagline: 'douglas caina, caina27, golang, flutter, bash, java, kotlin, react, angular, TDD, developer, blog',
    url: 'http://caina.dev.br',
    baseUrl: '/',
    onBrokenLinks: 'ignore',
    favicon: 'img/cool-lama.svg',
    organizationName: 'Caina Inc.',
    projectName: 'Caina website',
    githubHost: 'https://caina.github.com',
    stylesheets: [
        {
            href: 'https://fonts.googleapis.com/css?family=Anonymous+Pro&display=swap',
            type: 'text/css',
        },
        {
            href: 'https://fonts.googleapis.com/css?family=Lato:200,400,800&display=swap',
            type: 'text/css',
        }
    ],
    themeConfig: {
        docsUrl: '',
        navbar: {
            title: 'Caina',
            logo: {
                alt: 'Douglas caina - logo image',
                src: 'img/cool-lama.svg',
            },
            items: [
                { to: 'docs/a-list-of-usefull-links', label: 'Docs', position: 'left' },
                { to: 'blog', label: 'Blog', position: 'left' },
            ],
        },
        footer: {
            style: 'dark',
            links: [
                {
                    title: 'Docs',
                    items: [
                        {
                            label: 'Useful links',
                            to: 'docs/a-list-of-usefull-links',
                        },
                        {
                            label: 'Videos About Front',
                            to: 'docs/videos-about-front-end',
                        },
                    ],
                },
                {
                    title: 'Code',
                    items: [
                        {
                            label: 'Github',
                            href: 'https://github.com/caina',
                        },
                        {
                            label: 'GitLab',
                            href: 'https://gitlab.com/caina',
                        },
                    ],
                },
                {
                    title: 'Social',
                    items: [
                        {
                            label: 'Medium',
                            href: 'https://medium.com/@douglascaina',
                        },
                        {
                            label: 'Linkedin',
                            href: 'https://www.linkedin.com/in/douglascaina/',
                        },
                        {
                            label: 'Facebook',
                            href: 'https://www.facebook.com/caina27',
                        },
                    ],
                },
            ],
            copyright: `No Copyright © ${new Date().getFullYear()}. Built with Docusaurus.`,
        },
    },
    presets: [
        [
            '@docusaurus/preset-classic',
            {
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
                sitemap: {
                    changefreq: 'weekly',
                    priority: 0.5,
                },
            },
        ],
    ],
};

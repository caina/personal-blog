---
id: angular-advanced-snippets
title: Angular advanced snippets and concepts
sidebar_label: Angular Snippets 
---
![angular](/docs/angulares.jpg)
## Styles
```
:host {
    // css here is only available in the component
}
```

## Updating window.location.search without refresing page
```javascript
constructor(
private router: Router,
private activatedRoute: ActivatedRoute
){}

search(searchInput: string){
  this.router.navigate([], { 
    queryParamsHandling: 'merge',
    queryParams: {search: encodeUri(searchInput)}
  })
}
```

## Testing

Try always keep the AAA approach: `A`rrange, `A`ct and `A`ssert

- A component that tests an service is an `integration test`
- A unit test is the one that only tests is `component` or `service`

mockings:
- Dumies: they are placeholders for simple classes
- Stubs: Mocks with implementation
- Spies: Its a mock that keep track of the methods that has been called
- True mock: Checks if a specific service has been called with specific parameters

kind of tests:
- Isolated: single unit test, can construct class by hand
- integration test: we need to create a component (TestBed)
- - Shallow: only the main component
- - Deep: Rendering its child 

Quering the api
```
fixture.nativeElement.querySelector(a).textContent
fixture.debugElement.query(By.css('a')).nativeElement.textContent
```

### Ignoring unknown attributes:
```javascript
TestBed.configureTestingModule({
    declarations: [HeroComponent],
    schemas: [NO_ERRORS_SCHEMA] // ignore routerLink and others attributes
});
```

### Combining HTTP requests
```javascript
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/forkJoin';
getVehicles(): Observable<any> {
  const cars = this.http.get(CARS_ENDPOINT).map(res => res.json());
  const bikes = this.http.get(BIKES_ENDPOINT).map(res => res.json());
  
  return Observable.forkJoin([cars, bikes])
     .map(responses => {
        // responses[0] => cars
        // responses[1] => bikes
     });
}
```

### Mocking Window.Location
```typescript
function mockWindowLocation(search: string) {
    delete window.location;
    window.location = {
      ancestorOrigins: undefined,
      hash: '',
      host: '',
      hostname: '',
      href: '',
      origin: '',
      pathname: '',
      port: '',
      protocol: '',
      assign(url: string): void {
      },
      reload(forcedReload?: boolean): void {
      },
      replace(url: string): void {
      },
      search
    };
  }
```

### Mocking HttpRequest
for httprequests we can use a `RouterTestingModule` that enables us to spy, in this case Im using Jest

```typescript
it('test', () => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
    }).compileComponents();

    const navigateSpy = spyOn(TestBed.get(Router), 'navigate');
    expect(navigateSpy).toHaveBeenCalledWith(['route'], {
      queryParams: {search: '654654'}
    });
});
```

## Performance

### Updating Components
```typescript
@Component({
  selector: 'app-timer',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimerComponent {
    
    constructor(public timer: TimerService, private cdRef: ChangeDetectorRef) { }

    updateit(){
        this.cdRef.markForCheck();
    }
}
```

### Observable unsubscrition
```typescript
export class TimerComponent implements OnInit, OnDestroy {
 private countdownEndSubscription: Subscription = null;
 private countdownSubscription: Subscription = null;

   ngOnInit(): void {
     this.countdownEndSubscription = this.timer.countdownEnd$.subscribe(() => {
       console.log("do thing");
     })
 
     this.countdownSubscription = this.timer.countdown$.subscribe((data) => {
         console.log("do thing");
    });
   }
 
   ngOnDestroy(): void {
     this.countdownEndSubscription.unsubscribe();
     this.countdownSubscription.unsubscribe();
   }
}
```

## Patterns

### Behavior subject
```typescript
@Injectable()
export class TimerService {
    private countdownEndSource = new Subject<void>();
    public countdownEnd$ = this.countdownEndSource.asObservable();

    public test() {
        this.countdownEndSource.next()
    }
}

// usage
// <app-display [time]="timer.countdown$ | async"></app-display>
```


---
id: a-list-of-usefull-links
title: Interesting libraries and stacks
sidebar_label: Interesting Links 
---
This is a list of interesting things that I'm using or just interested.

- [Developer Roadmaps](https://roadmap.sh/roadmaps)
- [Gluu, Authentication](https://www.gluu.org/)
- [1 on 1 questions](https://github.com/VGraupera/1on1-questions)
- [theia-ide: Online IDE](https://theia-ide.org/)
- [ShiftIt, app for position windows on mac](https://github.com/fikovnik/ShiftIt)
- [How to secure anything](https://github.com/veeral-patel/how-to-secure-anything)

## DevOps
- [SdkMan - An client to download JDKs and SDKs like Java](https://sdkman.io)
- [Git auto complete for bash](https://github.com/chriswalz/bit)

## ORMS
- [Prisma](https://www.prisma.io/) : replacement for traditional ORMs, looks pretty awesome

## Task runners
- [Oya](https://oya.sh/) : Task runner to set up your project and compile go lang applications
- [TaskRunner](https://taskfile.dev/#/) : A more simple task runner

## Testing
- [Ginkgo: ](https://github.com/onsi/ginkgo): A BDD Testing Framework for Go 
- [Jest](https://jestjs.io/) : 🃏 Delightful JavaScript Testing 
- [Karate](https://github.com/intuit/karate): API test-automation 

### E2E
- [TestCafe](https://devexpress.github.io/testcafe/) : A node.js tool to automate end-to-end web testing
- [Zerocode](https://github.com/authorjapps/zerocode) Load test using java and junit
- [Artillery Community Edition](https://artillery.io/) load testing and functional testing toolkit
## Learning things
- [Scrimba, an IDE with video](https://scrimba.com/)
- [Katacoda](https://www.katacoda.com/learn)  : Interactive Browser Based Labs, Courses & Playgrounds
- [Udemy](https://www.udemy.com/) : Online Courses - Learn Anything, On Your Schedule
- [eggheadio](https://egghead.io/) : Short, instructional screencast video tutorials for web developers
- [Mathigon](https://mathigon.org/) : Learn matematics
- [Khan Academy](https://www.khanacademy.org/) : Free Online Courses, Lessons & Practice
- [free programming books](https://github.com/EbookFoundation/free-programming-books/blob/master/free-programming-books.md#professiona): Free ebooks about programming
- [Deep Learning](https://www.coursera.org/specializations/deep-learning#faq-list) : AI especialization
### Design Pattern
- [State Design Pattern](https://sourcemaking.com/design_patterns/state) : Design pattern book, really interesting.
- [design-patterns-for-humans](https://github.com/kamranahmedse/design-patterns-for-humans) : An ultra-simplified explanation to design patterns

### Front
- [Flexbox Zombies](https://mastery.games/p/flexbox-zombies) : A game that teatches Flexbox
- [Reactive Programming](https://egghead.io/courses/introduction-to-reactive-programming) : Introduction to Reactive Programming At Egghead
- [Fireship.io](https://fireship.io/) : Free classes about `Flutter` and `angular`
- [Learn Css grid on cssgridgarden](https://cssgridgarden.com/)

## Jobs
- [Eurobase People](https://www.eurobasepeople.com/) : some eurpean website to hire
- [Data Structures ](https://www.interviewcake.com/article/java/data-structures-coding-interview) : for Coding Interview -  Computer Science in Plain English Interview Cake
- [xing: germany linkedin](https://www.xing.com/)

## Code Exercise
- [Exercism](https://exercism.io/) : Website to do code excercise locally, pretty cool.
- [Hacker rank](https://www.hackerrank.com/) : Like exercisim but online. 
- [Gophercises ](https://gophercises.com/) : Coding exercises for budding gophers.
- [Codewars](https://www.codewars.com): the kata thing

## Front-end
- [Storybook](https://storybook.js.org/) : UI component explorer for frontend developers
- [GPU.JS](https://gpu.rocks):  GPU accelerated JavaScript
- [Grabient](https://www.grabient.com/) : Gradient generator
- [Presentational and Container Components](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0) : Post about presentational and container components
- [30 seconds of CSS](https://css.30secondsofcode.org/) : Snippet List 
- [MDX](https://mdxjs.com/) : This is fucking markdown with React <3
- [Unsplash](https://unsplash.com/) : Beautiful Free Images & Pictures
- [Feature Hub](https://feature-hub.io/): Create scalable web applications using micro frontends
- [World Vector Logo](https://worldvectorlogo.com/): Logos of brands
- [Free drawing stuff](https://www.freepik.com)
- [FREE SVG backgrounds](https://www.svgbackgrounds.com/)
- [Gridsome VUE - JAMStack, static websites](https://gridsome.org/)
- [Pattern CSS](https://github.com/bansal-io/pattern.css): library to fill empty background with beautiful patterns.
- [Keyframe](https://keyframes.app/): Animations, shadown and colours generator
- [Tailwind CSS](https://tailwindcss.com/) CSS utility class framework 
- [Arwes](https://arwes.dev/) SCI-FI framework 
- [CSS background patterns](https://www.magicpattern.design/tools/css-backgrounds/) cool backgrounds pure css 
- [FREE SVG icons royalt free](https://www.svgrepo.com/)
- [Random SVG avatars](https://github.com/boringdesigners/boring-avatars)
- [Tailwind material UI](https://github.com/creativetimofficial/material-tailwind?ref=material-tailwind)

## Flutter
- [Flutter examples](https://github.com/lohanidamodar/flutter_ui_challenges): a repo with flutter examples

## Serverless
- [SEED](https://seed.run/) : Deploy, manage, and monitor Serverless applications on AWS
- [Serverless](https://serverless.com/) : The Serverless Application Framework powered by AWS Lambda, API Gateway, and more
- [Netlify](https://www.netlify.com/) : All-in-one platform for automating modern web projects

## Golang
- [Learn go with tests](https://github.com/quii/learn-go-with-tests)
- [Go dev](https://go.dev)
- [Updating golang](https://github.com/udhos/update-golang)
- [Go microservices](https://github.com/micro/go-micro)

## Javascript
- [One line of JS, snippets in one line](https://1loc.dev/)
- [VITE: Next Generation Frontend Tooling](https://vitejs.dev/)

## API 
- [Traefik, golang gateway](https://containo.us/traefik/)
- [KrakenD open source gateway api](https://www.krakend.io/)

## Search engines
- [Angolia, personal search engine as a service](https://www.algolia.com/)
- [Typesense, angolia but open source](https://github.com/typesense/typesense)

## Design tools
- [Figma, online design](https://www.figma.com/)
- [AdobeXD](https://www.adobe.com/products/xd.html)

---
id: books
title: Books I've read 
sidebar_label: Books
---


# 2020

## You Ought to know Adam Wade
Just a regular guy talking about hist pation about story telling, interesting part: Everything that happen to him helped to create the person who he is.

## Ever tool's a Hammer
Complexidade: ele cria listas para saber o trabalho à frente e o que já foi feito, listas de listas pra deixar mais detalhado e simplificado.
Nenhum projeto começa bonito, você precisa de pelo menos 6 iterações pra poder mostrar para alguém, “quick and dirty” 
O autor começa pelas partes difíceis para não perder momentum mais tarde e pra terminar nas partes fáceis do projeto 

10 Bullets rules (pesquisar)
- work to code, work within the system
- Saicrat space, the studio is saicrat
- Be thorough 
- Give and get feedback 
- Get confirmation 
- Keep a list
- Always be nouling (pesquisar )
- Take responsibilities 

Feedbacks
Allow us to work together, without it, there is no way to improve.
Levels of feedback.
- recognition: Just saing good job
- encouraging: by saing why they’r job is important
- motivation: explain how they contribute to the result, it’s getting more of someone 
- Constructive feedback about problem
- Feedback for a team, a project that goes wrong is not just one person faulty 
- Confronting someone by personal problems,

(Pesquisar)
- the paradox of choice 
- análises paralises 

Explicando com desenhos:
Explicar uma ideia não funciona tão facilmente, ele propõe explicarmos pra um colega o que queremos fazer e deixá-lo desenhar, e conclui que dificilmente a pessoa acertaria o que queremos fazer.
Explicar com desenhos e mais fácil do que com palavras e que transmitir complexos sistemas com pessoas sem o mesmo background é quase impossível.

## We are bob, we are legion.
A von Neumann probe that self replicate in space, very interesting.

Bob became an replicant and leave earth.

## For we are many
the second book of the saga.

Bob finds a new species to take care, he find its way back to earth and deal with some terrorists. 
and find an alien specie that kills an eat everything

## All this worlds
The last book of the trillogy.
He finish his work of the first 2 books, kill all the aliens, fall in love, marry and became father.


# 2019 - 21 Livros.

## Como fazer amigos e influenciar pessoas
alguns pontos que tenho que aprender são os de não criticar as pessoas, evitar discuções pois não levam a nada e escute mais, deixe que as pessoa fale e se de conta por si só da ideia que eu quero passar.

Toda vez que tu precisar convencer alguém, faça com que ela comece dizendo: "Sim", isso a deixa mais sucetível à aceitar a sua ideia.

Princípio 1
: Sorria
Princípio 2
: Tenha interesse verdadeiro nas vidas das pessoas
Princípio 3
: seja um bom ouvinte, incentive as pessoas á falarem sobre elas
Princípio 4
: Admita seus erros
Princípio 5
: Aprenda o nome das pessoas
Princípio 6
: Faça a outra pessoa se sentir importante
Princípio 7
: tenha empatia, Entenda o problema dela e se coloque no seu lugar


> Um dos pontos interessantes é que as pessoas não melhoram com criticas, isso apenas deixa elas constrangidas e a mudança de habito se torna algo forçado e não tão natural.

Isso é um fato que me lembra das pesquisas climáticas e os feedbacks 360 que temos na empresa, possívelmente não são eficazes e apenas deixa o time mais desunido do que melhor.

Admitir erros:
---
Admitir que você está errado ajuda a fazer a outra pessoa à admitir que ela também estava errada, as vezes.
pode ajudar em uma discução, ou terminar ela. 

## Alien covenant
## unfuck yourself
## power moves
> Entrevista sobre o assunto com pessoas influentes: um ponto interessante é que empoderar funcionário os deixa motivados 
## sapiens
## the mountains of madness 
> Pesquisadores vão à uma montanha, acham espécies antigas que habitavam a terra, uma nevasca mata metade da equipe e o resto foge, um tempo depois voltam para investigar uma antiga cidade abandonada que encontraram. Vêem monstro e esculturas nas paredes mas nada passa de uma ilusão provocada pelas criaturas grotescas

Isso me faz imaginar meus artigos sobre codigos que são bosta, posso aplicar um pouco dessas ilusões por ver algo que é inconcebivel e criar narrativas ao invés de criar artigos chatos.

## Dune
> Livro sobre um duque que toma um golpe e seu filho (um profeta) vai morar em uma comunidade nas areias, comem > pimentas, ficam locões e entram em guerra, o fim foi uma bosta.

## Bad blood
>Livro sobre o caso da theranos. Me lembrou o claudio, um cara que engana as pessoas e faz falsas promessas, lida com as pessoas pelo medo e terror, aparentemente isso é uma coisa normal.
Fico pensando se isso não é um padrão, gestores que lideram pelo medo para esconder algum problema na empresa/pessoa.
## Dark matter: A novel. By Blake Crouch
> Livro sobre um cientista que aprende como criar a super posição quantica, fazendo uma pessoa poder navegar entre os multiversos da teoria das cordas, terminando o livro o personagem é perseguido por uma multidão dele mesmo de outras realidades.
O que achei interessante do livro é como nossas escolhas nos definem, voltar em uma esocolha e mudar ela não irá nos levar para uma nova realidade mas sim um novo eu. No fim das contas, quem somos nós? Se apenas com escolhas mudamos nossa personalidade ou até mosmo nossos arredores, morrer ou viver, faz pouca ou nenhuma diferença.
## Cesar last breath
> Livro sobre oxigênio e gases, a história de pesquisadores que descobriram os elementos.
coisas que achei interessante:
-> Oxigênio é usado para quebrar as moléculas de açucar nas mitocondreas.
-> Insetos respiram pelos seus exo esqueletos, como área e volume não crescem de forma homogênea, os insetos que ficam muito grande morrem sufocados, se tivessemos 2x mais oxigênio na atmosfera, aranhas seriam do tamanho de cachorros!
-> O pulmão é uma pelicula que joga moléculas de oxigênio no sangue, a reação dessa mistura troca a cor do sangue de azul para vermelho.

## Norse Mythology
> Estórias da cultura nordica, mitologia e enfins. Ótimo livro para entender o que acontecia naquela época com o povo

## The man who mistook his wife for a hat
> Nerociência e casos interessantes.
> Um dos casos no livro interessante é de um home que acorda desesperado dizendo que implantaram uma perna de um cadaver nele, ele não reconhece o próprio corpo como sendo dele. 
> *Fico imaginando se a sensação das pessoas transgeneras sobre os seus órgãos sexuais não pode ser uma variação dessa sindrome*

## The Martian
> um astronauta fica preso em marte quando uma tempestade atinge o acampamento que a equipe estava, a história é tão cientific accurated que fica impressionante.
 
## Ready player one: 17/07
> Um adolescente que vive em uma distopia entra em uma competição pela companhia que controla o sistema de entreterimento que o mundo inteiro vive, enquanto enfrenta a empresa IOI que tenta a qualquer custo ganhar a competição.
> *Gostei do narrador Willian whitan e como o livro leva em conta esse universo de jogos, e como ele deixa poucas pontas soltas e tenta amarrar toda a narrativa.*

## The adventures of Tom Strange
> Um livro ruim, meu deus...

## Rivals! Frenemies who changed the world
> Um livro sobre inimigos que mudaram a maneira com que o mundo funciona
> Adidas e puma são empresas de dois irmãos que tinham uma empresa juntos, mas durante a segunda guerra eles se separam e criam uma briga de familia bizonha. 

## Children of Blood and bone
> Zeil e seu irmão e uma princesa tentam trazer a magia de volta ao mundo enquanto que > o irmão da princesa merda tenta matar eles. 
> É um livro bom que fala de racismo, muito bom!

## American Gods
> Shadown é um cara que está pra sair da cadeia quando recebe um telefone que sua esposa está morta. No caminho para casa ele conhece um antigo deus (odin) que da emprego para ele e faz ele o acompanhar atrás de outros deuses antigos para uma batalha que estaria para acontecer em um futuro próximo.
> Quando a batalha se aproxima, "quarta feira" ou odim é morto pelos novos deuses, shadown embarca em uma jornada para levar o deus para o outro plano e nessa jornada descobre que é filho de odin.
> Não só que é filho mas que odin estava enganando a todos para entrarem em uma guerra, essa que o alimentaria e o deixaria mais poderoso que nunca.
> Shadown descobre e acaba com a guerra.
 
## The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Wi
DevOps stands for: Development + IT Operation (the guys that maintain servers, configure the networks change the user's mouse)

Its all about the so called 3 ways: 
- Work flow
- Reduce constraints and improve in the flow
- Continuos improvement and experimentation.

**Work Flow:** Sometimes the organization problem is to understand where the works come from and where is should go, I remember to see this when I have shit ton of work but never new from where to start, that's why we have the Kambam boards, to see the work and understand what of this work is WIP (Work in progress)

Wip is another recurrent topic in this book, the author speak of WIP as a treat for the company, imagine that in a factory, the goods come in one side, they become WIP as they go through the processing phases and end up as a final product, if a part of this process requires the employee 1h to finish, the rest of the work in que in this line has to wait for 1h in order the be executed.

If we are stuck in an inefitient working line, we need to understand what is the constraint, in the book example, it was an heat oven that was beeing used by 2 working lines. For that we must
1. Identify what is the constraint, what is blocking the project.
2. Exploit the contrait, making sure it is not allowed to wast more time.
3. Subordinate the constraint, the slowes process will dictate the speed of development.

There are 3 kind of work:
- The planned work.
- Internal work (some pet projects or improvements in the work line).
- Unplanned work, tasks that comes from nowhere and japerdise our sprints.
> In this point, we need to figure what is really work, what we should be really working on. In my projects, we usually check what in fact will add value to the client.

**Reducing constraints:** The solution in the book was to do a code freeze in all the projects and focus on what is more important, the even start to introduce Kambam to identify where is the WIP.

In this wave we must estabilish an upstream feedback loop: In Agile we do it in retrospectives. We can have monitoring tools and so on. The goal here is to shorten this iterations, and get more feedbacks. they even quote the toyota system: “Improving daily work is more important than doing daily work.”

**Continuos improvement:** This is a Lean culture, in every cycle of your project, we should give a step back, see what went wrong and improve it, not only bad things, but what is right as well. In the book, even though they manage to make the enviroment more reliable, the improved its process so they were able to deploy more ofen.

## Talking to strangers

- Default to true
we tend to believe that the people arround us are true and would not mean any harm to us.


## The Unicorn Project: A Novel about Developers, Digital Disruption, and Thriving in the Age of Data

The 5 ways:
- The First Ideal is Locality and Simplicity
How each part of your organizarion communicate with each other and how simple is to change part of the application, so if you have to communicate through 5 services to change a simple field, this is not the first ideal.
- The Second Ideal is Focus, Flow, and Joy
You must have fun while working, it generates flow for your team.
- The Third Ideal is Improvement of Daily Work
Like lean, find and improve your daily work is more important then work, like creating bash files to boot up docker images and run services.
This is commonly related to tecnical debt.
- The Fourth Ideal is Psychological Safety
Is all about be able to speak your mind without afraid of being fired. Its the same safety as for a factory worker.
> Sensei Oneil: Blamming culture destroy innovation.
- The Fifth Ideal is Customer Focus
Create produts that are important to the user, not for managers.

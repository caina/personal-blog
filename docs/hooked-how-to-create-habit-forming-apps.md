---
id: hooked-how-to-create-habit-forming-apps
title: Hooked Creating Habbit formimg apps
sidebar_label: Hooked 
---

Livro sobre como deixar os usuários mais interessados na sua plataforma, uma plataforma não vai virar habit-forming se não forçar os usuários à interagir diáriamente.

uma opção é fazer os usuários gastarem tempo montando uma plataforma, elas irão tentar proteger aquilo que lhes da uma sensação de dominança

Qualquer aplicação que diminua o trabalho envolvido em uma tarefa ganhará aceitação imediata, o quanto mais simples, melhor.
> Podemos ver aqui o nubank que diminui o uso de bancos

##### Entre os fatores que geramam simplicidade temos:

| Ação              | Efeito
|-------------------|-------
| Tempo             | Quanto tempo uma tarefa ser executada
| Dinheiro          | Quanto essa ação vai me custar
| Esforço físico    | Quanto trabalho vai me dar fazer isso
| Ciclos cerebrais  | Nível de trabalho mental envolvido na tarefa
| Aceitação social  | Quanto fazer essa ação vai ser aceito socialmente
| Rotina            | Quanto a ação vai se tornar uma rotina

Imagino que o sucesso está em um nivelamento entre as ações citadas aqui e as recompensas que podemos oferecer ao usuário.
Ex: Caso ele ganhe muito conhecimento do uso da plataforma, ele não se importará de executar uma ação diáriamente, como no caso o Audible, que exige que escutemos os livros enquanto que eles nos da de retorno o conhecimento.

##### Motivos para usar um sistema:
- Prazer para evitar a dor
- Esperança para evitar o medo
- Aceitação social para evitar rejeição

Recompenças 
--- 
Da tribo
: quando os usuários se recompensam entre si, as pessoas esperam reconhecimento em seu meio

Para si
: As pessoas caçam informações, elas precisam ganhar coisas que elas imaginem lhes dar vantagens

##### Existem atalhos mentais para as noças ações:

Euristicos: Imagine um jarro com 10 cookies e um com apenas 2, qual vai ter mais valor para os usuários?

> Imagino que isso se compare com os convites que o orkut dava, não era uma plataforma nova mas era difícil de ser aceito. Podemos fazer os mesmo usando os sitema de grupos por nível que tinhamos planejado.

Framming: Os usuários dão mais valor se a plataforma se vender como tal. Pense nisso como as marcas de roupas caras que são iguais as demais mas se vendem como especiais.



Trigger
---
O que faz o usuário voltar pra sua plataforma, como fazer eles se engajarem mais, podendo ser pagos como anuncios, organicos que é a influencia de amigos 





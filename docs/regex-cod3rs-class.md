## Regex pela Cod3rs

- [link da aula](https://www.cod3r.com.br/courses/take/regex-cursos-de-expressoes-regulares/lessons/9335655-o-que-e-expressao-regular)
- [website para testar](https://regex101.com/)

**Starting with regexs**

```javascript
const text = 'Casa bonita é a casa amarela da esquina com a Rua ACASALAR.'

const regex = /casa/gi
text.match(regex) // ['Casa', 'casa', 'CASA']
text.match(/a b/)) // ['a b']
```

## Meta characters
   
| character | name | meaning |
| ----------| -----| --------|
| `.`       | Dot. | Any character |
| `[]`      | Group| allowed group of characters | 
| `[^]`     | Group Negation | forbidden character group |
| Quantificatives | | 
| `?`       | Optional | Zero or one |
| `*`       | Star | Zero or more | 
| `+`       | Plus | More than one |
| `{n,m}`   | Keys | From n to M |
| Anchors | | 
| `^`     |  | Beginning of the line |
| `$`     | Dolar | End of line |
| `\b`    | Border | Beginning or end of Word |
| Generics | |
| \  | Scape | Scape the meta-character |
| (pipe) | Or | Oo operation | 
| `()` | Group | Defines a group |
| `\1..\9` | Rearview | Restore an predefined grou|
 
 
```javascript
const text = '0,1,2,3,4,5,6,a.b c!d?e'
const regexPonto = /\./g

text.split(regexPonto) // ['0,1,2,3,4,5,6,a', 'b c!d?e']
```

### (Dot) ⏺ meta character
It's a wild card for one position, it can be any character at the string

```javascript
const text = '1,2,3,4,5,6,7,8,9,0'
text.match(/1.2/g) // ['1,2']
text.match(/1..,/g) // ['1,2,']

const grades = '8.3,7.1,8.8,10.0'
grades.match(/.\../g) //['8.3', '7,1', '8.8', '0.0']
```

> Be aware it doesnt solve the problem for breaking lines, eg: 'Bom\ndia' with a `/./gi` returns ['B','o','m','d','i','a'] without the \n 
> some languages implements it as a flag dotall `/exp/s` but not Javascript


### white spaces
we can use the \t to find tabs and \s for spaces'
```javascript
const text = `
ca r
r o s!                            
`
text.match(/ca\tr\nr\to\ss!/) // ca\tr\nr\to s!
```

### Using the pipe Or
A simple way to have an alternative in a condition.

```javascript
const text = "você precisa responder sim, não ou não sei!"
text.match(/sim|não|sei/g) // ['sim', 'não', 'não']
// to ignore the special character
text.match(/sim|n.o|sei/g) // ['sim', 'não', 'não']
```

## Flags 🚩

| 🚩 | ❓ |
|----- |---- |
| `//g` | is for global, search for all matchs |
| `//i` | ignore cases |

some examples in JS 👌
```javascript
const text = 'Carlos assinou o abaixo-assinado'
text.match(/C|ab/) // just C
text.match(/c|ab/i) // C again because of the ignore case flag
text.match(/ab|c/gi) // C and ab for the global case is active
```

## How to execute regex in some languages

running with javascript
```javascript
const text = '0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g'
const regexNove = RegExp('9')

regexNove.test(text) // true 
regexNove.exec(text) // executes and return true

const regexLetras = /[a-f]/g
text.match(regexLetras) // String method
text.search(regexLetras) // 20 -> index where it starts
text.replace(regexLetras, 'Found') // 0,1,2,3,4,5,6,7,8,9,Found,Found,Found,Found,Found,g
text.split(regexLetras)// ['0,1,2,3,4,5,6,7,8,9',',',',',',',',',',',',']
```

examples in golang

```golang
func main() {
    text := "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f"

    regex9, _ := regexp.compile("9")
    fmt.Println(regex9.MatchString(text)) // true
    fmt.Println(regex9.FindString(text)) // true: 9
    fmt.Println(regex9.FindStringIndex(text)) // true: [18 19]

    regexLetras, _ := regex.Compile("[a-f]")
    fmt.Println(regexLetras.FindAllString(text, 20)) // true [18 19] -> first 20 -> [a b c d e f]
    fmt.PrintLn(regexLetras.ReplaceAllStrinhs(text,  "Found")) // 0,1,2,3,4,5,6,7,8,9,Found,Found,Found,Found,Found
}
```

## Basic characters

literal strings search

```javascript
const text =  '0,1,2,3,4,5,6,a.b c!d?e'

const regexVirgula = /,/
text.split(regexVirgula) // ['0','1','2','3','4','5','6','a.b c!d?e']
text.match(regexVirgula) // [',']

```

## Sets
to define a set we use the charaters: `[]`.

> Inside of a set, the meta characters, eg: '.$+*?-' works as a literal word, not as a wildcard, so we don't need to scape them

some basic examples
```javascript
const text = '1,2,3,4,5,6,a.b c!d?e[f'
const regexPares = /[02468]/g
text.match(regexPares) // ['2', '4', '6']
```

and we can use a literal letter and a next character inside of a set
```javascript
const text = 'João não vai passear na moto'
text.match(/n[aã]/g) // ['nã', 'na']
```

we can negate a set using `^`
```javascript
const text = '1,2,3,4,5,6,a.b c!d?e[f'
text.match(/[^0-9]/g) // a.b c!d?e[f
// same as 
text.match(/\D/g)
```

using shorthans with symbols
```javascript
const text = '1,2,3,4,5,6,a.b c!d?e[f'
text.match(/[^\d!\?\[\s,\.]/g) //a.b c!d?e[f
// is the same as
text.match(/[^\d]/g) //a.b c!d?e[f
```

### Using intervals over sets
```javascript
const text = '1,2,3,4,5,6,a.b c!d?e[f'
text.match(/[a-z]/g) // all letters
text.match(/[A-Z1-3]/gi) // ['1','2','3','a','b','c','d','e','f']
```

### Shorthands
Instead of defining groups for everything, we can use a shorthand to solve the same problem
giving the following string

"1,2,3,4,5,6,a.b c!d?e\t-\nf_g"

| 🚩 | ❓  |  {} |
|--- |-----|-----|
| `/\d/g`| digits `[0-9]` | `[1,2,3,4,5,6]` | 
| `/\D/g` | not digits `[^0-9]` | `[',',',',',',',',\t']` |
| `/\w/g` | characters from `[a-zA-Z0-9]` | `[1,2,3,4,5,6,a,b,c,d,e,f,_,g]` |
| `/\W/g` | not characters: `[^a-zA-Z0-9]` | `[',',',',',',',',\n,\t]` |
| `/\s/g` | spaces: `[\t\n\r\t\f]` | `[\n,\t,' ', \r]` |
| `/\S/g` | not spaces: `[^ \t\n\r\t\f]` | every no space character |

## Quantifiers
ways that we can use `{0,2}` and so on

finding where the character "o" is optional (?)
```javascript
const text = 'de longe eu avistei o fogo e uma pessoa gritando: FOGOOO'
const text2 = 'There is a big fog in NYC'

const regex = /fogo?/gi

text.match(regex) // [fogo, FOGO]
text2.match(regex) // [fog]
```

### When you have multiple ocurrencies (+)
 ```javascript
 const text = 'de longe eu avistei o fogo e uma pessoa gritando: FOGOOO'
 const text2 = 'There is a big fog in NYC'
 
 const regex = /fogo+/gi
 
 text.match(regex) // [fogo, FOGO]
 text2.match(regex) // null
 ```

### Using the plus operator to get a group
```javascript
const text = "Os números: 0123456789"
text.match(/\d/g) // [0,1,2,3,4,5,6,7,8,9]
text.match(/\d+/g) // ['0123456789'] <- one element
// to not sum
text.match(/\d+?/g) // [0,1,2,3,4,5,6,7,8,9]
```

### Zero or more (*)
 ```javascriptm
 const text = 'de longe eu avistei o fogo e uma pessoa gritando: FOGOOO'
 const text2 = 'There is a big fog in NYC'
 
 const regex = /fogo*/gi
 
 text.match(regex) // [fogo, FOGO]
 text2.match(regex) // [fog]
 ```

### Keys for specific numbers {n,m}
```javascript
const text = "O João recebeu 120 milhões apostando em 6 9 21 23 45 46"
text.match(/\d{1,2}/g) //[12, 0, 6, 9, 21, 23, 45, 46]
text.match(/[0-9]{2}/g)//[12, 21, 23, 45, 46]
text.match(/\d{1,}/g) //[120, 6, 9, 21, 23, 45, 46]
//or better using borders
text.match(/\b\d{1,2}\b/g)//[6, 9, 21, 23, 45, 46]

// for words
text.match(/\w{7}/g) // [recebeu, apostand] <- for having õ, milhões doesnt include for not being part of \w
text.match(/\b[\wõ]{7}\b/g) // [recebeu, milhões]
```

### Greedy or lazy
quantifiers are greedy by default

```javascript
const text = `<div>Conteudo 01</div><div>Conteudo 02</div>`
text.match(/<div>.+<\/div>/g) // [<div>Conteudo 01</div><div>Conteudo 02</div>] <- greedy
text.match(/<div>.+?<\/div>/g) // [<div>Conteudo 01</div>, <div>Conteudo 02</div>] <- greedylazy
```

## Groups
They are defined using `()`

```javascript
const texto = `O José Simão é muito engraçado... hehehehehehe`
texto.match(/(he)+/g) // ["hehehehehehe"]
```

To match urls: 
```javascript
const text = `
http://www.site.info
www.escola.ninja.com.br
google.com.ag
http://www.site.info

and some other ones eve. don`

text.match(/(https?:\/\/)?(www.)?\w+\.\w{2,}(\.\w{2})?/g) // all of them
```



## Challanges 💩
some regex challanges 
### file list
how many .mp3 can we find in this string?

```javascript
const text ="lista de arquivos mp3: jazz.mp3, rock.mp3, podcast.mp3, blues.mp3"
text.match(`/\.mp3/g`) // ['.mp3', '.mp3', '.mp3', '.mp3'] 
// for the hole word
text.match(/\w+\.mp3/g) // ['jazz.mp3', 'rock.mp3', 'podcast.mp3', 'blues.mp3']
```

### 3 spaces
create an regex that finds 3 spaces in a string

```javascript
const text = 'a   b'
text.match(/a\s{3}b/)
// or even better
text.match(/a\s+b/)
```

### regex CPF
get the CPF numbers from a string

```javascript
const aprooved = `
CPF dos aprovados:
- 600.567.980-12
- 765.998.345-23
- 333
`
aprooved.match(/\d{3}\.\d{3}\.\d{3}-\d{2}/gm // ["600.567.980-12", "765.998.345-23"]
```

### regex phone numbers
create a list of phone numbers

```javascript
const phones = `
Lista telefônica:
- (11) 98756-1212
-98765-4321
-987654321
`
phones.match(/[(\d{2})]?\s?\d+-?\d+/gm) // ["(11) 98756-1212","98765-4321","987654321"]
```
### regex Email
for emails in general
```javascript
const text = `
Os emails dos convidados são:
- fulano@cod3r.com.br
- xico@gmail.com
- caina@mbr.mx
- sampaio@sample.de
e assim por diante com emails como d@d.com`
text.match(/[\w.]+@[\w+.]+/gm) // ["fulano@cod3r.com.br","xico@gmail.com","caina@mbr.mx","sampaio@sample.de","d@d.com"]
```


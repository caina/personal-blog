---
id: videos-about-architecture
title: Interesting videos about code Architecture
sidebar_label: Architecture. 
---

## [Microservices + Events + Docker = A Perfect Trio](https://www.youtube.com/watch?v=sSm2dRarhPo)
**to be seen**

## [10 tips for failing badly at microservices by David schmitz](https://www.youtube.com/watch?v=X0tjziAQfNQ)
**to be seen**

## [Making sense of MVP (Minimum Viable Product)](https://www.youtube.com/watch?v=0P7nCmln7PM)
Really interesting talk, I used to think that MVP was just the minimum for the application to run, but its actually an diferent product.
The ideia is to create a simplified version of the app, but as a final product! And as you make sense of the need of the user, you'll create better versions of the same app until it becomes the final product, and sometimes change It's concept to better suit your client.
*Users never know what they want.*

![making sense of mvps](../static/docs/making-sense-of-mvp-talk.png)

## [Complete Beginner's Guide to Big O Notation](https://www.youtube.com/watch?v=kS_gr2_-ws8)
**to be seen**

## [Making Architecture Matter - Martin Fowler Keynote](https://www.youtube.com/watch?v=DngAZyWMGR0)
> In a nutshell: it’s the one that enables you to compose your functionalities or extends and over time gain speed. I have one article explaining some similar concept for front end: [Arquiteturas escaláveis para SPAs com Angular](https://medium.com/cwi-software/arquiteturas-escal%C3%A1veis-para-spas-com-angular-6d55b3549166)

## [GOTO 2014 • Microservices • Martin Fowler](https://www.youtube.com/watch?v=wgdBVIX9ifA)

## [Design patterns patterns](https://www.youtube.com/playlist?list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc)

## [Qualities of a Highly Effective Architect Keynote by Venkat Subramaniam](https://www.youtube.com/watch?v=QeKheNfO3Yg&feature=youtu.be)
**to be seen**

## [Zach Barth: "Zachtronics: Ten Years of Terrible Games" | Talks at Google](https://www.youtube.com/watch?v=Df9pz_EmKhA)

## [Clean code 4 ](https://drive.google.com/open?id=1UAU1iV5NLIU-c8qUTl-8N129zpKyLM4f)

## [Clean code 5](https://drive.google.com/open?id=13IXSrxuDrPg5o2JboGqhyp9-pssthRXa)

## [Clean code 6 pt 1](https://drive.google.com/open?id=119qeioaqwyYiK1pPbmJlhw1itBFyUk0z)

## [Clean code 6 pt 2](https://drive.google.com/open?id=1ZNDb3yI1fF4wqTLfcQNPEqfpts4kfEpS)

## [Clean code 7](https://drive.google.com/open?id=1z3AWYqCfl1VM-Q-oTIREHpoFTbJtMjb3)

## [Clean code 8](https://drive.google.com/open?id=1btvndGowh3Evp9HNQDkaldIVrsdU4xd8)

## [Clean code 9](https://drive.google.com/open?id=1a0UQYyA-i707ZbJr-7ZHmyCrKGeyub7Y)

## [Clean code 10](https://drive.google.com/open?id=1PzuliNU17nIqzCFRS8wSO6BoKLcHKc50)

## [Clean code 11](https://drive.google.com/open?id=13J7vwqkbYrC5FMQo8z28QhzjT6w3dWg_)


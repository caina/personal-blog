---
id: videos-about-agile
title: Interesting videos about Agile
sidebar_label: Agile. 
---

A pattern we come across this videos is how DevOps is changing our world, they talk about:
1. Culture
1. Development Cycles
1. Feedback
1. DevOps

So in culture, its all about stoping this thinger point stuff, were making mistakes is a sin, alowing us to experiment 
and giving freedon, we can come to new "disruptives" solution, it's almost impossible to create when you are scared of 
lose your job.

Development cycles is more to understand if the code you are delivering is performing well, if the things you are delivering
are really valuables to the clients. This is important not only to keep track on your progress, but to elevate the self esteen
of the team. 

Feedbacks of the team, you need to check if your team is not burning out, if they are happy, all this factors are intertwine.

DevOps: its more about the Lean movement comming to development and stuff, more below.

## [Agile is Dead • Pragmatic Dave Thomas](https://youtu.be/a-BOSpxYJ9M)
- find where you are
- Take a small step toward your goal
- Adjust your understanding based on what you learned
- Repeat
- When faced with two alternatives, take the path that makes future changes easier

## [Agility ≠ Speed - Kevlin Henney](https://youtu.be/kmFcNyZrUNM)

- Time points in agile is pure especulation, there is no way go to give a real estimation of a task. 
- The act of developing creates knowledge.
- We should focus in doing and learning from our mistakes than just plan, that's why agility comes from the latin "to do"
- He speaks about this [Amdahl's law](https://en.wikipedia.org/wiki/Amdahl%27s_law).
![Amdahl](/docs/Amdahl.png)
Being T=Time and N= Number of developers, stating with no developers the curve goes to infinit, and as you add the curve is created

![boys OODA loop](/docs/boysoodaloop.png) [boyss OODA loop](https://en.wikipedia.org/wiki/OODA_loop)
- Big teams produce big softwares, you can’t make a small software having tons of developers
- work on cycles
- Conways’s law: organizations which design system are constrained to produce designs which are copies of the communication structures of these organizations. We have seen that this fact has important implications for the management of system designs. A design effort should be organized according to the need for communication.
![conways airplane](/docs/Conways-2.png)
You can translate this airplane into Conway law as following:
![conways airplane](/docs/Conways-1.png)

- “In the long run every programmer becomes rococo. Then rubble” , Alan perlis
> Why do cars have breaks? So you can slow down. Or it has breaks so we can go faster

## [Extreme Programming 20 years later by Kent Beck](https://youtu.be/cGuTmOUdFbo)
- Inspiration is preparation + panic, and neighter alone works 
- The extreamming programming borrow a lot from the Toyota system, It’s basicaly lean for software
- Kent Beck wrote the first version of JUnit in a 4h flight!
- The Extreme programming book was written in a train from zurik to monique.

I've been reading various lean manifestos and this has never cross my mind, that in the end of the day, agile is just lean for development, its basicly a feedback loop where you have to analyse your problem, execute, measure the results and improve, the lean startup chaged this improve to "pivot".
Some people say that we should do this loop while coding, or every time. 

Other aspect of the talk that I liked is the way that Kent talk about how this "extreme programming" is bananas and the entire system should not be followed as a rule, but to be adapted, as following the system, we should improve it in a way that makes sense to our projects.
![Spring cycles](/docs/lean-model.png)

## [Brewing Agile 2017: James Priest: Sociocracy 3.0](https://www.youtube.com/watch?v=HvFZL2tGLiM)

## [Agile Software Architecture - Ian Cooper](https://www.youtube.com/watch?v=3YCIw3gewFE)

## [Extreme programming in a nutshell - Rachel Davies, angular connect](https://www.youtube.com/watch?v=1FLaxNvmCc8)

## [How Facebook software is made: the ultra agile approach - Jonathan rose ](https://www.youtube.com/watch?v=CmcE1pvfWHc)
In short, its the well known agile + extreme programming + DevOps, nothing special. The video is cool though.

**Waterfall: Plan and document up front**
- Engineering discipline applied to software
- Device schedule and budget
- Extensive documentation
- Dedicated specialised team
**Agile Development**
- Software produced in small increments
- Release to public quickly
- Quick response to change request
- Little planning, little documentation
- Individual responsible to everything

Your code is the documentation! And the responsibility for the project is in everyones hand, not only to the management.
> Focus groups: it’s a group of people to ask what the client needs or what products must be.

**Waterfall estimatives:**
In the 70 to 80s in real softwares, in average a software engineer produces 10 lines of code per day. Nowadays this has increased to 20.
As an estimative, 50% of the softwares produced is never used, because it doesn’t work.
In agile:
- self organised and cross-functional team
- continuous improvement
- rapid and flexible
- Never work for companies that has a testing team,
to spot a non agile company, during interview, ask the following question: “Can I write code today, and have end users using it in the end of the day?”. If they say that you are crazy, they are not using agile.
Most of the companies are using feature toggle and delivering for part of they costumers.

> Your code is the documentation! And the responsibility for the project is in everyones hand, not only to the management.

> *Focus groups*: it’s a group of people to ask what the client needs or what products must be.

**Waterfall estimatives:**
In the 70 to 80s in real softwares, in average a software engineer produces 10 lines of code per day. Nowadays this has increased to 20.
As an estimative, 50% of the softwares produced is never used, because it doesn’t work.
In agile:
- Self organised and cross-functional team
- Continuous improvement
- Rapid and flexible

## [Scaling Agile @ Spotify with Henrik Kniberg](https://www.youtube.com/watch?v=jyZEikKWhAU)
He spoke about how giving awereness about the end result of your projects, and no much orders to a team can increase its innovation. Its the idea of a leader ordering to build a bridge, or asking to cross the river.

![alignment enables autonomy spotify video](/docs/spotify-autonomy-team.png)
This is a common problem in past projects, I had an low autonomy project, and the quality of the code base was really bad, until the point that nobody wanted to refactor anything, in the other hand, I worked for a unscoped project and no feature was being developed.
 
- Productivity = effort x competence x environment x motivation2
- high alignment enables high autonomy (we need a big picture of the problem )
> This concept is interesting, we should not give a developer an recipe , but a goal. There is nothing more inproductive as a developer, not being able to create. I might write an article about this in the future.
- No hands off, you are responsible for the feature, and bugs related 
- “If everything is under control, you are going too slow” - 100% control = 0% motion 
- Don’t punish people for mistakes. (Like a children 🤔)
- Limit blast radius of failures by decoupling architecture 
- Bugs in production if they are controlled, (user groups) you’ll learn faster and improve faster
- Add a board about the 3 top impediments, or most common
- Define an definition of awesome: a list of things the drive us toward a goal,thing unreachable yet 
- Culture > process. *A good culture heals an broken process*. 
- Culture is the things we do without noticing, you are the culture 

## [Brewing Agile 2017: Marcus Hammarberg: "Building A Hospital with Agile and Lean](https://www.youtube.com/watch?v=5tTy5IMqzDE)

## [Brewing Agile 2017: Kate Terlecka: "Fix Your Business, Culture Will Follow"](https://www.youtube.com/watch?v=EHnnH8TAn1k)

## [The Difference Between Lean and Agile](https://www.youtube.com/watch?v=aUd3xTdtXqI)

## [(Brewing Agile 2014) Tomas Trolltoft: How to make your team happier and perform better](https://www.youtube.com/watch?v=-zmZ4X3VNJM)

## [Agile Product Ownership in a Nutshell](https://www.youtube.com/watch?v=502ILHjX9EE)

## [GOTO 2015 • Is SAFe Evil? • Lars Roost & Henrik Kniberg](https://www.youtube.com/watch?v=TolNkqyvieE)

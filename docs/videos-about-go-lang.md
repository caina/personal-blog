---
id: videos-about-go-lang
title: Interesting videos about Go Lang
sidebar_label: Go lang. 
---

## [Twelve Go Best Practices - Francesc Campoy](https://www.youtube.com/watch?v=8D3Vmm1BGoY)
**to be seen**

## [Golang UK Conference 2016 - Mat Ryer - Idiomatic Go Tricks](https://www.youtube.com/watch?v=yeetIgNeIkc)
**to be seen**

## [2 4 Google Understanding Go Interfaces](https://www.youtube.com/watch?v=F4wUrj6pmSI)
**to be seen**

## [Opening keynote: Clear is better than clever - GopherCon SG 2019](https://www.youtube.com/watch?v=NwEuRO_w8HE)
**to be seen**

## [Building an Basic API with gRPC and Protobuf](https://www.youtube.com/watch?v=Y92WWaZJl24)
its kind of Java RMI for golang.

## [Intro to gRPC: A Modern Toolkit for Microservice Communication](https://www.youtube.com/watch?v=RoXT_Rkg8LA&t=280s)
**to be seen**

## [7 common mistakes in Go and when to avoid them by Steve Francia (Docker)](https://www.youtube.com/watch?v=29LLRKIL_TI)
**to be seen**

## [GopherCon 2019 Lightning Talk: Tim Raymond - Parsing Expression Grammar](https://www.youtube.com/watch?v=rwzhu5YegpE)

## [GopherCon 2019: Mat Ryer - How I Write HTTP Web Services after Eight Years](https://www.youtube.com/watch?v=rWBSMsLG8po)

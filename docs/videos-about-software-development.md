---
id: videos-about-software-development
title: Interesting videos about Sofware Development
sidebar_label: Software Development. 
---

## [Thinking Small is Changing Software Development](https://youtu.be/YCQMiFF9QXM)
> How Thinking Small is Changing Software Development Big Time • Sander Hoogendoorn

## [TDD for those who don't need it](https://youtu.be/a6oP24CSdUg)
> Gopher con

## [TDD, Where Did It All Go Wrong](https://youtu.be/EZ05e7EMOLM)
- TDD slows you down 
- Lean software development drop tests
- Duct tape programming guys are winning because they ship the code 
- when we return to our tests we don’t understand what’s going on 
- Sometimes the test don’t really do anything, just testing the mocks (that’s great)
- ATDD??
- blaming how made the Test red
- Developers don’t see the value of them 
- TDD is dead long live testing (article by David heinemier)
- Avoid testing implementation, test behaviors 
- A trigger for a test should be an requirement, not a new class 
- you do not test code, you test behaviors 
- During the red green phase, we should do code as the duck tape developers do: as shit as possible, and refactor later
- [TDD is dead. Long live testing.](https://dhh.dk/2014/tdd-is-dead-long-live-testing.html) 
> Summary:
- write dirty code to get to green, then refactor 
- No new test for refactored internal and private methods
- Both develop and accept against test written on a port
- Add integration tests for coverage of ports to adapters
- Add system test for end to end confidence
 - Don’t mock internal, private or adapters

## [The Three Laws of TDD](https://youtu.be/qkblc5WRn-U)
He compares tests to doctors cleaning  they’r hands before a surgery, there was a time that it was not a common practice.
you should not be good in debugging, you must be good at coding.
code examples are like tests, they must explain the code, and they should not be dependent.
It’s not funny to write tests after the code because we already know how the code works. When you come across a hard code to test. its because its not designed to be tested.
If you write a test first, is hard to write a code that is hard to test, because its designed for that.

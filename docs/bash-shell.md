---
id: bash-shell
title: Bash Script snippets and tips
sidebar_label: Bash Snippets 
---

[Bash scripts repo](https://gitlab.com/caina-learning/shell-script)

the header of a bash script is a definition of the interpreter for the machine, its usually called shebang (`!#`)

if its not informed, it still can work, but the executer will take an interpreter as the default, its better to especify always

`#!/bin/bash`

### Commands usefull
- Kill all node appsn `Killall node npm`
- one `>` send a content to a file, `>>` appends
- stop all docker containers `docker stop $(docker ps -a -q)` or to remove: `docker rm $(docker ps -a -q)`
- Wait for server to be up and running:
```bash
wait_for_it () {
  local LIMIT=20
  while ! curl -s --fail $1 >/dev/null
  do
      if (($LIMIT == 0))
      then
        exit 1;
      fi

      echo "app has not started yet, atempts left: $LIMIT"
      sleep 5
      ((LIMIT--))
  done
  echo "ready to go"
}
```

### Navigating with CD
- `cd ../` goes one folder up
- `cd ~/` goes to home
- `cd -` goes back to last location
example
```
cd ~/ 
you navigate to home

cd folder/inner/folder
you go into various folder at once

cd -
you are back to home
``` 

### Tempalte of a bash file
- the Shebang (`#!/bin/bash`)
- Comments/file header
- Global variables
- Functions
- - Dont forget to use `local` variables
- Main script contents
- Exit with an status
- - multiples exits during the application

##### listing items in a folder:
the star is taking any that has .jpg
`VAR=$(ls *.jpg)`

## Assining the output of a command in a variable

`SERVER_NAME=$(hostname)`
its always in the rounds
an old alternative would be like:
```bash
SERVER_NAME=`hostname`
```

## Variable 
All variables by default are GLOBAL!
and they have to be defined before used

to use local variables, use the keyword: `LOCAL` before the variable name, as:
`local LOCAL_VAR=1` and they are scoped to theyrs functions.

### names
take care with special variables like "PATH"

|valid| invalid
|---|---|
|FIST3LETTERS|3LETTERS|
|FIRST_THREE_LETTERS|first-three-letters|
|firstThreeLetters|first@three@letters|

important: while assiging values to an variable, do not use spaces

## File operators
conditions:
```bash
[ -e /etc/passwd ]
```
|operator|result|
|---|---|
|-d FILE| this if file is directory|
|-e FILE| true if file exists|
|-f FILE| true if file exists and is a regular file|
|-r FILE| true if file is readble by you|
|-s FILE| true if file exists and is not empty|
|-w FILE| true if file is writable by you|
|-x FILE| true if the file is executable by you|

### String operators
|operator|result|
|---|---|
|-z STRING | true if string is empty|
|-n STRING | true if string is not empty|
|STRING1=STRING2| true if the strings are equals|
|STRING1!=STRING2| true if the strings are not equals|

### number operators
|operator|result|
|---|---|
| arg1 -eq arg2| first arg equals to second|
| arg1 -ne arg2| if arg1 is not equal second|
| arg1 -lt arg2| arg1 less than arg2|
| arg1 -le arg2| arg1 less than or equal to arg2|
| arg1 -gt arg2| arg1 greater than arg2|
| arg1 -ge arg2| arg1 greater than or equal to arg2|

## If condition
```bash
if [condition-is-true]
then
    command 1
    command 2
fi
```
else:
```bash
if[ ]
then
    -command
else
fi
```

else if:
```bash
if[]
then
    -command
elif[]
then
    -command
else
    -command
fi
```

### Using if to check if it contains value
```bash
instanceNo=`unzip -p -qq $1 ${fileunderzip} | grep instance-no`
if [[ ${instanceNo} =~ "645234345" ]]; then
    echo "dale"
fi
```

## for loops
```bash
for VARIABLE_NAME in ITEM1 ITEM2
do
    -command
    -commnad
done
```

to iterate through a fori loop is:
```bash
for ((i=1;i<=50;i++));
do
    echo $i
done
```

## Positional Parameters
you can access the parameters using
`script.sh parameter1 parameter2 parameter 3`

|parameter|position|
|----|----|
|$0|script.sh|
|$1|parameter1|
|$2|parameter2|
|$3|parameter3|

to acess all of them you can use: `$@`
for loop throught all the inputs:
```bash
for USER in $@
do
    echo "Archiving user: $USER"
done
```

### user input
to read commands accepts STDIN
`read -p "PROMPT" VARIABLE`
for example:
```bash
read -p "enter an user" USER
echo "archiving user: $USER"
```

## Exit status
Every command returns and exit code from 0 to 255
being 0 sucess and grater than it an error
___
to explicity define a return code
`exit 0`
`exit 2`
`exit 255`
you can use it always to exit the script


### checkong the exit status
to get the previous command exit status, use:
`$?`
as an example:
```bash
ls /not/here
echo "$?"
```

example checking google ping
```bash
HOST="google.com"
ping -c 1 $HOST
if [ "$?" -eq "0" ]
then
    echo "$HOST reachable"
else 
    echo "$HOST unreached"
fi
```

this can be as well given to an variable, like:
`RETURN_CODE=$?`

### && and ||

`&&` is the "and" operator, it just execute the next if the previous was succesfull
`mkdir /tmp/bak && cp test.txt /tmp/bak`

`||` is the "or" and will be executed if the previous command fail
`cp test.txt /tmp/bak || cp test.txt /tmp`

we can rewrite the previous code as:
```bash
HOST="google.com"
ping -c 1 $HOST && echo "$HOST reachable"
```

### the semicolon
Separate comands with the semicolon to ensure all get executed

`cp test.txt /tmp/bak ; cp test.text /tmp`
it does not check the status of the previous commands, ots the same as using the commands in new lines

## Functions
there is 2 ways to create a function
they must be declare before being executed, but if you call a function inside a function it would be ok.

```bash
function function-name(){
    # code goes here
}

function-name(){
    # code goes here
}
```

and to call a function is just call the name as an example:
```bash
function hello(){
    echo "Hello"
    now
}
function now(){
    echo "its ${date -%r}"
}
hello
```
___
Exit status:
functions have an exit status and can be explicity define on the `exit` or explicity by the result of the last command executed.

to avoid funtions to exit the shell script, add an `set -e` to its content, like this:
```bash
function sample(){
    set -e 
 

    exit 1
}
sample
echo $?
```
___
Function parameters:
functions can acess parameters using $1, $2 or $@ to all,
and can acess global variables.

### Parameters
functions parameters works pretty much like the shell parameters
take this as an example:

```bash
function hello() {
    echo "Hello $1"
}
hello Jason
>> output: Hello Jason
```
so its not necessary to provide any bracket.
to loop through all the inputs of a function is simple as:
```bash
function hello(){
    for NAME in $@
    do
        echo "Hello $NAME"
    done
}
hello Jason Dan Ryan
```

#### Exit status of a function
to get the exit status of the function is simple as:
```bash
my_function
echo $?
```

a good example of how to use it would be:
```bash
function backup_file() {
    if [ -f $1 ]
    then
        local BACK="/tmp/$(basename ${1}).$(date +%F).$$"
        echo "Backing up $1 to ${BACK}"
        cp $1 $BACK
    fi
}
backup_file /ect/hosts
if [ $? -eq 0 ]
then
    echo "Backup Succeeded"
fi
```

## WildCards
its a character os string to use for pattern matching, we can use them with commands like `ls, rm, cp`, common called globbing operation.

### Most used wildcards

|matcher| example|
|-------|--------|
|(`*`) Matches zero or more characters | *.txt; a*; a*.txt|
|(`?`) matches exacly one character | ?.txt (f.txt) ; a? (as); a?.txt (as.txt) |
|(`[]`) character class, matches any character included between the brackets | - ca[nt]* ;can; cat; candy; catch|
| (`[!aeiou]*`)to exclude characters, use the `!`| baseball; cricket; |
| (`[a-g]*`) matches all the files on a range | a,b,c,d,e,f,g|
| (`[3-6]*`) matches number from 3 to 6 |1,2,3,4,5,6|
| (`[[:alpha]]`) alphanumerics| a; A;b;B |
| (`[[:alnum]]`) Alphanumerics| A;b;1;|
| (`[[:digit]]`) Digits| 0 to 9|
| (`[[:lower]]`) lower case names| a;b |
| (`[[:space]]`) spaces| ` ` |
| (`[[:upper]]`) uppercase| A;B |

to scape wildcards is just add a backslash `ˆ*\?` will match `done?`

to use this wildcards in a bash script you can do something like this:

```bash
cd /var/www
for FILE in *.html
do
    cp $FILE /var/www-just-html
done
```

## Case statements
same as switch
```bash
case "$1" in 
    start|START)
        /usr/sbin/sshd
        ;;
    stop)
        kill $(cat /var/run/sshd.pid)
        ;;
    [yY][yY][eE][sS]
        echo "you said yes, that makes no sense"
        ;;
    *)
        echo "Usage: $0 start|stop"; exit 1
        ;;
esac
```

## Logs

|facility| severity|
|--------|--------|
| kern | emerg|
| user |alert|
| mail |crit|
| daemon | error|
| auth |warn|
| local0 -> local7 |notice|
| 1 |info|
| 1 |debug|

the most severe message is `emerg` and the lowest is `debug`. most syst store the logs at:
`/var/log/message` or `/var/log/syslog` but it depends of your system.

> - To use them, the simple command is: `logger "message"`
> - but to define an severity would be: `logger -p local0.info "message"`
> - to attach the name of your script: `logger -t myscript -p emerg "message"` and to add the PID, include an `-i` before the `-t` in the previous script

if in addition to store this logs you want to print them in the console, include an `-s` to your script

```bash
logit () {
    local LOG_LEVEL=$1
    shift
    MSG=$0
    TIMESTAMP=$(date +"%Y-%m-%d %T")
    if [ $LOG_LEVEL = 'ERROR'] || $VERBOSE
    then
        echo "${TIMESTAMP} ${HOST} ${PROGRAM_NAME}[${PID}]: ${LOG_LEVEL} ${MSG}"
    fi
}
```

*Shift* shift the positional parameters to the left, removing the first paramter that has been used in "LOG_LEVEL" and setting the rest of the variables to "MSG"

so using this script would be like:`fetch-data $HOST || logit ERROR "Could not fetch from $HOST"`

## While loops
If a command return an non 0 exit status, the loop stops,
a basic usage would be:
```bash
while [ CONDITION_IS_TRUE ]
do
    command
done
```
> important: commands do not change the while condition!

to create a kind of service, we can use the *sleep* command for a given second.
```bash
INDEX=1
while [ $INDEX -lt 6 ]
do
    echo "Creating project -${INDEX}"
    mkdir /usr/local/project-${index}
    ((INDEX++))
done
```

an example checking the user imput:
```bash
while [ "${CORRECT}" != "y" ]
do
    read -p "ENTER YOUR NAME:" NAME
    read -p "IS ${NAME} CORRECT?" CORRECT
done
```

example of an script that checks if a server is down, being `app1` our application url
```bash
while ping -c 1 app1 >/dev/null
do
    echo "app1 still up"
    sleep 5
done
echo "app1 down, continue"
```

if you read a file through a loop, bash will read it word by word, to read line by line we would do something like it:

```bash
LINE_NUMBER=1
while read LINE
do
    echo "${LINE_NUM}: ${LINE}"
    ((LINE_NUM++))
done < /etc/fstab
```

or to read a file through a command is like:
```bash
grep xfs /etc/fstab | while read LINE
do
    echo "xsf: ${LINE}"
done
```
and to split it into multiple lines:
```bash
FS_NUM=1
grep xfs /etc/fstab | while read FS MP REST
do
    echo "${FS_NUM}: file system: ${FS}"
    echo "${FS_NUM}: file system: ${MP}"
    ((FS_NUM++))
done
```

and to stop a while loop without exiting the script use a `break`, and to jump to next iteration, use `continue`

## Debugging
to debug, there is some options to print helpfull hints.
to see all the options, type `help set | less`

This is trace debbuging:
| comand | help|
| -------| ----|
| `#!/bin/bash -x` | set at the header of the file an option to print it|
| `set -x`   | prints commands as they execute|
| `set +x`| to stop debugging|
| `-e`, `#!/bin/bash -ex | -xe | -e -x| -x -e` | makes the script to exit immediatly of an script ends with an non zero value | 
|`-v` | prints the shell commands as they are in the script|

example of output:
```bash
./31.sh
+ LINE_NUMBER=0
+ read LINE
+ echo '0: root: 123'
0: root: 123
+ (( LINE_NUMBER++ ))
+ read LINE
+ echo '1: main: 321'
```
> commands with a plus sign are the commands executed by the script

## Grep
can be used to filter things, like folder list: `ls | grep search`

## Sed: stream editor
stream are pipe operations. the sed command is used to perform text transformation on these streams, for example:

- Substitute some text for other text.
- Remove lines.
- Append text after given lines.
- Insert text before certain lines.

> sed is used programmatically not iteractively.

the most common use is to find and replace in command line, like: 
```bash
echo 'Swight is a the assistant regional manager.' > manager.txt;
sed 's/assistant/assistant to the/'  manager.txt
```
> output: Dwight is ths assistant to the regional manager.

but it does not change the value of the manager.txt file.
sed is case sensetive, to igonre it, end the command with in  "i", like: `s/TEXT/replace/i`

> by default sed just replace values one time per line, to replace this behaviour to all the file, add an `g` as follow: `/s/text/replace/g` and to replace just the x number of the occurrence you can provide a number in the end, like: `s/text/replace/2`, so only the second occurrence is going to be replaced.

we can use the option `-i` to modify the current file.
to avoid having to scape any `/` character, you can use something like: `s#/home/jason/#/export/users/jasonc#`, in other words, you don't need to use the `/` as a delimiter, you can use any special character.

### Delete or remove data
```bash
sed '/this/d' file.txt
```
this will delete every word `this` in the file.txt
- removing with regex: `sed '/ˆ#/d' config`
- multiple sed commands: `sed '/^#/d ; /^$/d' config`
- multiple sed commands `sed -e '/^#/d -e /^$/d' config`
- we can use address in the beggining of the command like: `sed /Group s/apache/httpd/ config` and it'll only apply for those who start with `/Group`
- Address with line range: `sed '1,4 s/run/execute' config`

> You can create an sed file, with multiple instructions for each line and suply the file to the operation using `-f`, like: `sed -f sed-script.sed config`

### Using sed to get xml tag elements of an zipped element
This was usefull so why not take notes? the code is taking all the values from a zipped xml file, without unzipping it, we search for the tags `<title name="language">`
this returns an list of elements with the same tag, that's why the ` | head -1` is used for, it takes the first element of the list.
```bash
tagValue=$(unzip -p -qq $1 "fileToUnzip" | sed -n '/title name="language"/{s/.*<title name="language">//;s/<\/title.*//;p;}' | head -1)
```

### Using sed to rename bash variables

```bash
filename="search for me"
newFile=$(sed "s/search/replace/g" <<< "$filename")
```

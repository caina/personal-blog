---
id: howto-javascript-nestjs-crud-operation
title: Creating a CRUD operation using NEST JS
sidebar_label: Crud operation
---
This is a super section, with multiple links, follow the ones fit best for you.

> Pro tip use a todo list to never miss a point.

- [ ] [Install NestJS](https://docs.nestjs.com/first-steps)
- [ ] [Creating the scafold](#scafold)
- [ ] [Configuring Database](#database)
- [ ] [Controller](#controller)

## Scafolding
Although nestJs has a great CLI, lets use a more BDD driven aproach.
Under `src` create the following structure.
```
- src
--/database
---ormconfig.ts
---database.providers
---database.module
---/my-feature-entity
----my-feature.entity.ts
----my-feature.provider.ts
---/migrations
--/my-feature
---/api
----controller.ts
---/services
---/dto.ts
```

the reason for separating the database layer from the business domain is so that we can use [TypeORM](https://github.com/typeorm/typeorm).

## Database
you'll need for local developent a docker compose file with your choice of database
- [for Mysql](howto/docker/docker-compose/mysql-57)
> Important! If you are using mysql, install the "mysql2" driver instead of the normal one.

```typescript
import { join } from 'path';
import { ConnectionOptions } from 'typeorm';
import * as fs from 'fs';

const conn = {
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'root',
  database: 'idk_console_db_dev',
  ssl: undefined,
};

const connectionOptions: ConnectionOptions = {
  type: 'mysql',
  ...conn,
  synchronize: false,
  dropSchema: false,
  migrationsRun: true,
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  migrations: [join(__dirname, 'migrations/*{.ts,.js}')],
  cli: {
    migrationsDir: 'src/database/migrations',
  },
};

export default connectionOptions;
```
*ormconfig.ts*

Its better to keep this file a side so we can use it to call typeOrm over its configuration, more in [configuring TypeOrm](#configure-typeOrm)

### Creating the entity
you can read more about entities at Typeorm documentation, here is just a mockup, although I strongly recommend always use soft delete.

```typescript
import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity } from '../baseEntity';

@Entity('my_feature')
export class MyFeature {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'sample', nullable: false })
  sample: string;

    @Column({
        name: 'created_at',
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    createdDate: string;
    
    @Column({
        name: 'modified_at',
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    modifiedDate: string;
    
    @DeleteDateColumn({
        name: 'deleted_at',
    })
    deletedAt?: Date;
}
```
*my-feature.entity.ts*

### Setting up the provider
you can use enuns for the provider names.

```typescript
import { Connection, Repository } from 'typeorm';
import { MyFeature } from './my-feature.entity';

export const myFeatureProviders = [
  {
    provide: 'MYFEATURE_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(MyFeature),
    inject: ['DATABASE_CONNECTION'],
  },
];
```
*my-feature.provider.ts*

```typescript
import { createConnection } from 'typeorm';
import connectionOptions from './ormconfig';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => await createConnection(connectionOptions),
  },
];
```
*database.providers*

### Adding the module
```typescript
import { Module } from '@nestjs/common';
import { databaseProviders } from './database.providers';
import { myFeatureProviders } from './my-feature/my-feature.providers';

@Module({
  providers: [...databaseProviders, ...myFeatureProviders],
  exports: [...databaseProviders, ...myFeatureProviders],
})
export class DatabaseModule {}
```
*database.module.ts*

and thats it, database is ready to roll.

## Controller.

```typescript
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { MyFeatureDto, CreateMyFeatureDto } from '../dto';
import { MyFeatureService } from '../services/my-feature.service';

@Controller('api/myfeature')
export class MyFeatureController {
  constructor(private myFeatureService: MyFeatureService) {}

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  async create(@Body() dto: CreateMyFeatureDto): Promise<MyFeatureDto[]> {
    await this.myFeatureService.create(dto);
    return this.myFeatureService.find(undefined);
  }

  @Delete('/:id')
  async delete(@Param('id') id: string): Promise<MyFeatureDto[]> {
    await this.myFeatureService.delete(id);
    return this.myFeatureService.find(undefined);
  }

  @Get()
  @UsePipes(new ValidationPipe({ transform: true }))
  async find(@Query() request): Promise<MyFeatureDto[]> {
    return this.myFeatureService.find(request.search);
  }

  @Put()
  @UsePipes(new ValidationPipe({ transform: true }))
  async update(@Body() dto: MyFeatureDto): Promise<MyFeatureDto> {
    return await this.myFeatureService.update(dto);
  }
}
```
*my-featureController.ts*

# Configure TypeOrm
over package.json, add the following commands:
```package.json
"scripts": {
    "typeorm": "ts-node -r tsconfig-paths/register ./node_modules/typeorm/cli.js --config src/persistence/ormconfig.ts",
    "migrations": "npm run typeorm migration:run"
}
```



---
id: howto-docker-redis
title: Docker Redis
sidebar_label: Docker Redis
---

To add redis in a docker compose use

```dockerfile
#  sudo apt install docker docker-compose
#  docker-compose up -d

version: '3.6'

services:
  redis:
    container_name: rate-limit-redis
    image: redis
    ports:
      - 6379:6379
```

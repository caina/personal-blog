---
id: howto-docker-mysql57
title: Docker Mysql
sidebar_label: Docker Mysql
---
Mysql 5.7 with an entry point

```dockerfile
#  sudo apt install docker docker-compose
#  docker-compose up -d

version: '3.6'

services:
  db:
    container_name: consent-db
    image: mysql:5.7
    restart: always
    ports:
      - 3306:3306
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'
    volumes:
      - ./schemas.sql:/docker-entrypoint-initdb.d/consent.sql

```

---
id: howto-kotlin-architecure-resillient4j-rate-limit
title: Rate limit
sidebar_label: Rate limit
---

Rate limit is usefull to block malicious users, this section does not cover the HEADERS or store client information if the purpose is to charge the custumer for api usage.

## Step 1:
Install resillient 4J and [Redis](/docs/howto/docker/docker-compose/redis) 
```
implementation("io.github.resilience4j:resilience4j-kotlin:1.7.0")
implementation("io.github.resilience4j:resilience4j-spring-boot2:1.7.0")

implementation("org.springframework.data:spring-data-redis")
implementation("es.moki.ratelimitj:ratelimitj-core:0.7.0-RC3")
implementation("es.moki.ratelimitj:ratelimitj-redis:0.7.0-RC3")
```
build.gradlew

## Infrastructure
Rate limit uses redis to calculate ammount of calls per key, to install follow the following guide:
- [Adding regis do docker](/docs/howto/docker/docker-compose/redis)

## Configuration
Add a configuration file

```kotlin
@Configuration
class RateLimitConfig {
    @Value("\${rate-limit.maxThroughput}")
    var maxThroughput: Long = 0

    @Value("\${rate-limit.slidingWindowSize}")
    var slidingWindowSize: Long = 0

    @Value("\${vcap.services.rate-limit-cache.credentials.host:localhost}")
    lateinit var redisHost: String
}
```
rateLimitConfig.kt

## Add the applycation.yml rate limit config
```yml
rate-limit:
  maxThroughput: 100
  slidingWindowSize: 60  # in seconds for TIME_BASED window-type
```
application.yml

## Create an custom notation
```kotlin
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class RateLimit(
    val rateKey: String
)

@Aspect
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
class RateLimitAspect(rateLimitConfig: RateLimitConfig) {

    private val rules: Set<RequestLimitRule> = Collections.singleton(RequestLimitRule.of(Duration.ofSeconds(rateLimitConfig.slidingWindowSize), rateLimitConfig.maxThroughput))
    private val redisUrl = "redis://${rateLimitConfig.redisHost}:6379"
    private val factory: RequestRateLimiterFactory = RedisRateLimiterFactory(RedisClient.create(redisUrl))

    @Around("@annotation(com.volkswagen.identitykit.security.RateLimit)")
    fun rateLimit(joinPoint: ProceedingJoinPoint): Any? {
        val key = getKey(joinPoint)

        val requestRateLimiter = factory.getInstance(rules)
        val isLimitReached = requestRateLimiter.overLimitWhenIncremented(key)

        if (isLimitReached) {
            throw TooManyRequestsException()
        }
        return joinPoint.proceed()
    }

    private fun getKey(joinPoint: ProceedingJoinPoint): String? {
        val rateLimitAnnotation = getRateLimitAnnotation(joinPoint)
        return SpelExpressionParser().parseExpression(rateLimitAnnotation.rateKey)
            .getValue(joinPoint, String::class.java)
    }

    private fun getRateLimitAnnotation(joinPoint: ProceedingJoinPoint): RateLimit {
        val signature: MethodSignature = joinPoint.signature as MethodSignature
        return signature.method.getAnnotation(RateLimit::class.java)
    }
}
```
RateLimitAspec.kt

## Using the annotation:
We need a key to count the calls, the `args[1]` is going to make the annotation to use that value ("userId") as the key for rate limmiting.

```kotlin
@RestController
class Controller {
    @PostMapping("/enpoint/{app}/{userName}")
    @RateLimit("'storeV2-'+args[1]")
    fun storeV2(@PathVariable app: String, @PathVariable userId: String): ResponseEntity<Any> {
        // ...
    }
}
```
somecontroller.kt

## Aspect Test
Tests for the custom annotation

```kotlin
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RateLimitAspectTest {

    @Autowired
    lateinit var client: TestRestTemplate

    @Test
    fun `should not limit usages`() {
        val response = client.getForEntity("/rate-limiter-mock/rate/some-user", String::class.java)
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
    }

    @Test
    fun `should limit on reaching threashold`() {
        var response: ResponseEntity<String>? = null
        repeat(11) {
            response = client.getForEntity("/rate-limiter-mock/rate/some-user", String::class.java)
        }

        assertThat(response!!.statusCode).isEqualTo(HttpStatus.TOO_MANY_REQUESTS)
    }
}

@RestController
class RateLimitController {
    @GetMapping("/rate-limiter-mock/rate/{userId}")
    @RateLimit("'rate' + args[0]")
    fun rate(@PathVariable userId: String): ResponseEntity<String> {
        return ResponseEntity.ok(userId)
    }
}
```
RateLimitAspectTest.kt

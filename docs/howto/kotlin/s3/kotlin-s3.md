

```
 implementation("com.amazonaws:aws-java-sdk-s3:1.11.822")
 ```
build.gradlew.kts


```kotlin


@Suppress("FunctionName")
@ExtendWith(SpringExtension::class)
@ActiveProfiles("integration", "stop-scheduled-import")
@SpringBootTest
internal class S3PackageLoaderMockAmazonS3IT(
    @Autowired val s3PackageLoader: S3PackageLoader,
    @Autowired val amazonS3: AmazonS3,
    @Value("\${bucket.bucket}") private val bucketName: String,
    @Value("\${bucket.prefix}") private val prefix: String,
    @Value("\${full_export_tag}") private val fullExportTag: String
) {

    @TestConfiguration
    open class Configuration {
        @Bean
        open fun amazonS3() = mockk<AmazonS3>(relaxed = true)
    }

    @Test
    fun `loads files fails when transfer is aborted`() {
        val packageName = "DEL.KT6KTZ.TP.D.GGO.ZIP.000640"

        every { amazonS3.getObject(bucketName, "$prefix/$fullExportTag/$packageName") }.throws(AbortedException("Nope"))

        s3PackageLoader.setAmazonS3(amazonS3)

        assertThrows<S3TransferFailed> {
            s3PackageLoader.withPackage(packageName) {}
        }
    }

    @Test
    fun `returns false if the configured s3 bucket does not exists`() {
        every { amazonS3.listObjects(bucketName, "$prefix/003605").objectSummaries }.returns(emptyList())
        val validS3Configuration = s3PackageLoader.isValidS3Configuration("003605")

        assertFalse(validS3Configuration)
    }

    @Test
    fun `returns true if the configured s3 bucket exists`() {
        every { amazonS3.listObjects(bucketName, "$prefix/003605").objectSummaries }.returns(listOf(S3ObjectSummary()))
        val validS3Configuration = s3PackageLoader.isValidS3Configuration("003605")

        assertTrue(validS3Configuration)
    }
}
```

```kotlin
@Component
class S3MediaStore(
    private val s3Bucket: S3Bucket
) : MediaStore, MediaUploader {

    private val amazonS3: AmazonS3
        get() = s3Bucket.amazonS3

    companion object {
        private const val DOCUMENTS = "documents"
        private const val NOT_FOUND = 404
    }

    private fun upload(folder: String, name: String, inputStream: InputStream, size: Long) {
        try {
            if (!exists(folder, name)) {
                putObject(folder, name, inputStream, size)
            }
        } catch (e: AbortedException) {
            throw MediaUploader.UploadAborted(e)
        }
    }

    private fun putObject(folder: String, name: String, inputStream: InputStream, size: Long) {
        amazonS3.putObject(
            s3Bucket.bucketName, "${s3Bucket.prefix}/$folder/$name", inputStream,
            ObjectMetadata().apply {
                contentLength = size
            })
    }

    override fun uploadDocument(name: String, inputStream: InputStream, size: Long) =
        upload(DOCUMENTS, name, inputStream, size)

    override fun copyDocument(name: String, out: OutputStream) = copy(DOCUMENTS, name, out)



    private fun exists(folder: String, name: String) =
        amazonS3.doesObjectExist(s3Bucket.bucketName, "${s3Bucket.prefix}/$folder/$name")
}

data class S3Bucket(val amazonS3: AmazonS3, val bucketName: String, val prefix: String)
```
S3MediaStore.kt

---
id: design-pattern
title: Design patterns
sidebar_label: Design Patterns 
---

_Abstract ou interface, qual a diferença?_

Classes abstratas não podem ser instanciadas, apenas extendidas, interfaces precisam ser implementadas.

## Strategy pattern.
### Composição vs Herança
Herança é orientação a objetos simples, enquanto composição é uma camada que chama os métodos necessários em uma certa ordem

Imagina que tu tem uma classe pato, e uma pato do mato, dando um override no voar tu monta teu pato. Agora tu tem um novo pato "pato das trevas" que voa como o pato do mato, ai tu já tem problemas na herança.

```
               +----------+
               |  Duck    |
               +----------+
               |  Fly()   |
               |  Eat()   |
               |          |
               ++--------++
                |        |
                |        |
+----------+    |        |  +----------+
|  Duck2   | <--+        +->+  Duck 3  |
+----------+                +----------+
|  Fly() +----------------> |  Fly()   |
|  Eat()   |                |  Eat()   |
|          |                |          |
+----------+                +----------+

```
Ao invés disso, `Fly` e `Eat` seriam interfaces na classe `Duck`, como um "IQuackBehavior", Fazendo com que a herança seja menor mas é mais fácil de modificar caso um dos patos tenha uma implementação diferente

```
interface IQuackBehavior { Quack() }
```

> Podemos também injetar o "quack" e display na classe, o que facilita nos testes,
> e deixar a classe mais segura, caso use o injector no construtor. 

```
class Duck {
  IFlyBehavior;
  IQuackBehavior;
  IDisplayBehavior;
  
  public Duck(IFlyBehavior,IQuackBehavior,IDisplayBehavior){
    this.IDisplayBehavior = IDisplayBehavior;
    this.IQuackBehavior = IQuackBehavior;
    this.IDisplayBehavior = IDisplayBehavior;
  }
  
  public void Fly() {
    this.IFlyBehavior.fly();
  }
}
```

> Isso trabalha com o Open closed principle, já que agora tu só precisa criar novos Behaviors mas o pato será
> sempre o mesmo pato! Isso é genial.

## Observer Pattern.

Essencialmente é um: Push vs Poll

O problema que estamos tentando resolver é uma classe fazendo pulling em outra perguntando o estado dela: "você já atualizou?" E isso escalado em um grande grupo de classes perguntando se elas já atualizaram pode ser um grande problema de performance.

Observer pattern faz com que movemos de polling para push, a classe fica responsável em avisar aos que se subscreveram que ela atualizou.

Ele separa em quem observa e quem é observado. É uma relação de 1 para N.

```
  +-----------------+       +------------+
  |IObservable      |       | IObserver  |
  +-----------------+       +------------+
  |Add(IObserver)   |       | Update()   |
  |Remove(IObserer) +---------->         |
  |notify( )        |       |            |
  +-------+---------+       +-------+----+
          ^                         ^
+---------+----------+      +-------+------------+
| ConcreteObservable |      |  ConcreteObserver  |
+--------------------+      +--------------------+
| Add()              |      |  Update()          |
| Remove()           |      |                    |
| Notify()         <--------+                    |
|                    |      |                    |
| GetState()         |      +--------------------+
| SetState()         |
+--------------------+

```
SetState e GetState são metodos genéricos para que nossa aplicação faça algo, como em um chat em que pegamos dados do banco e setamos o estado de uma sala.

O Observer também fica com uma referencia da classe observada, para poder executar métodos, porém ela não deve nunca ter acesso ao stream de dados, quem observa jamais pode ter a habilidade de atualizar diretamente o estado do observado.

Algo mais ou menos assim.
```
// New ConcreteObserver(Concrete)

WheatherStation station = new WheatherStation();
PhoneDisplay display = new PhoneDisplay(station);

station.add(display);
station.notify("yei!")
```

```
Interface IObserver {
 Update()
}

Interface IObservable {
  Add(IObserver)
  Remove(IObserver)
  Notify()
}

class WeatherStation implements IObservable {
 private List<IObserver> observers;
 
 public void add (IObserver o){
  this.observers.add(o)
 }
 
}
```


## Decorator Pattern

É um Wrapper em uma classe, adicionando responsabilidades adicionais ao objeto dinamicamente.


---
id: flutter-usefull-resources
title: Flutter
sidebar_label: Flutter 
---

*Flutter being deleted from the macosx catalina*: `sudo spctl --master-disable`


Nossos componentes podem receber e dar assign em props da seguinte maneira:

```
class Teste {
  String input
  Teste(this.input)
}
```

isso da assign e recebe o input, mas ele agora é obrigatório, podemos também ter os valores opcionais nomeados e com valores default da seguinte maneira:

```
class Teste {
  String input
  Teste({this.input = "loco"})
}
Teste(input: "oi")
```

e caso você queira um valor opcional não nomeado, da pra fazer assim:

```
class Teste {
  List<String> input
  Teste([this.input = const []])
}
```

## Mixins
Mixins are the way to extends classes in dart, following by the syntax:
`class Person with ... {}`


## Ways to user provider
Passing the context and not attaching the content to the component
```[dart]
ChangeNotifierProvider(
    builder: (ctx)=> MyProvider
)
```


Simplified version attaching the content to the provider. Better for lists
```[dart]
ChangeNotifierProvider.value(
    value: MyProvider
)
```

## Routing:

```[dart]
MaterialApp(
      routes: {
        "/": (ctx) => TabsScreen(favoriteMeals: _favoriteMeals),
        MealsListingScreen.routeName: (ctx) =>
            MealsListingScreen(_availableMeals),
        MealsDetailsScreen.routeName: (ctx) => MealsDetailsScreen(
            toggleFavorite: _toggleFavorite, isMealFavorite: _isMealFavorite),
        FilterScreen.routeName: (ctx) =>
            FilterScreen(saveFilters: _setFilters, savedFilters: _filters),
      },
      onGenerateRoute: (settings){
        print(settings.arguments);

        if(settings.name == '/meal-detail') {
          // return the page
        }else if(settings.name == '/something-else'){
          // return MaterialPageRoute(builder: (ctx) => CategoriesScreen());
        }

        return MaterialPageRoute(builder: (ctx) => CategoriesScreen());
      },
      //  404 page
      onUnknownRoute: (settings) {
        return MaterialPageRoute(builder: (ctx) => MealsCategoriesScreen());
      },
);
```

## Providers
You can use 2 ways to read the product provider
or:
```[dart]
final product = Provider.of<Product>(context);
return  Image.network(
              product.imageUrl,
              fit: BoxFit.cover,
            )
```
or: 
```[dart]
 return Consumer<Product>(
    builder: (ctx, product, child) =>Image.network(
            product.imageUrl,
            fit: BoxFit.cover,
            label: child,
        )
    child: NEVER_CHANGE()
 )
```

using more than one provider: 

```[dart]
MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Products()),
        ChangeNotifierProvider.value(value: Cart()),
      ], 
      child: ...
      )
```

You can as well pass a provider for just a tinny part of your component by wrapping it with Consumer:

```[dart]
class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);

    return ClipRRect(
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          title: Text(
            product.title,
            textAlign: TextAlign.center,
          ),
          trailing: IconButton(
            color: Theme.of(context).accentColor,
            icon: Icon(Icons.shopping_cart),
            onPressed: () {},
          ),
          // here we are listening for events on favorite click
          leading: Consumer<Product>(
            builder: (ctx, product, child) => IconButton(
              color: Theme.of(context).accentColor,
              icon: Icon(
                product.isFavorite ? Icons.favorite : Icons.favorite_border,
              ),
              onPressed: product.toggleFavorite,
            ),
          ),
        ),
      ),
    )}
}
```

## Keys
`key: ValueKey(id),`

## Design

apple bottom navigations
```
Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        key: ValueKey("timer-add-button"),
        onPressed: () {},
        backgroundColor: Colors.red,
        child: Icon(Icons.play_arrow),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 1,
        type: BottomNavigationBarType.shifting,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.shutter_speed, color: Colors.amber),
              title: new Text('spread')),
          BottomNavigationBarItem(
              icon:
                  Icon(Icons.local_drink, color: Color.fromARGB(255, 0, 0, 0)),
              title: new Text('calculate')),
        ],
        onTap: (index) {},
      ),
      body: SafeArea(
        child: Center(
          child: Text("aa"),
        ),
      ));

```

## border radius container
```
Container(
  decoration: BoxDecoration(
      border: Border.all(color: Colors.grey, width: 1.0),
      color: Color.fromRGBO(220, 220, 220, 1),
      borderRadius: BorderRadius.circular(20)),
),
```

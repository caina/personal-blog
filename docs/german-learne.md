---
id: german-learne
title: Learning german
sidebar_label: German learning
---


|  DE    |  EN     |
|--------|---------|
|Sein    | to be   |
|Ich bin | I am    |
|Du bist | You are |
|Er ist  | He is   |
|Sie ist | She is  | 
|Es ist  | It is   |
|Plural  |         |
|Wir sind| We are  |
|Ihr seid| You are |
|Sie sind| They are|


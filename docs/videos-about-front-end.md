---
id: videos-about-front-end
title: Interesting videos about Front-end
sidebar_label: Front-end. 
---

## [Node Summit 2017 - PLATFORM AS A REFLECTION OF VALUES: JOYENT, NODE.JS, AND BEYOND - Bryan Cantrill](https://vimeo.com/230142234)
*To be seen*

## [Rust, WebAssembly, and the future of Serverless by Steve Klabnik](https://www.youtube.com/watch?v=CMB6AlE1QuI)
- edge computing: servers near its customers 
- Rust and web assembly 
- Fastly

## [Building WebGPU with Rust The new foundation for graphics and compute](https://fosdem.org/2020/schedule/event/rust_webgpu/)

## [The Birth & Death of JavaScript](https://www.destroyallsoftware.com/talks/the-birth-and-death-of-javascript)

## [The Science of Great UI](https://www.youtube.com/watch?v=nx1tOOc_3fU&feature=youtu.be)
*To be seen*

## [CSS Grid Changes EVERYTHING - Amazing Presentation](https://www.youtube.com/watch?v=7kVeCqQCxlk)
some examples of how to use it, looks intersting.

## React

### [React 16.6 - What's New? Theory + Practice](https://www.youtube.com/watch?v=BnasObkCGtQ)

Context: main.js
```js
export const AuthContext = React.createContext({
  isAuth: false,
  toggleAuth: () => {}
});

class App extends Component {
  state = {
    isAuth: false
  };

  toggleAuth = () => {
    this.setState(prevState => {
      return {
        isAuth: !prevState.isAuth
      };
    });
  };

  render() {
    return (
      <AuthContext.Provider
        value={{ isAuth: this.state.isAuth, toggleAuth: this.toggleAuth }}
      >
        <Login />
        <Profile />
      </AuthContext.Provider>
    );
  }
}
```

using context
```js
const login = props => (
  <AuthContext.Consumer>
    {authContext => {
      return (
        <button onClick={authContext.toggleAuth}>
          {authContext.isAuth ? 'Logout' : 'Login'}
        </button>
      );
    }}
  </AuthContext.Consumer>
);
```

> Memo

```js
const login = props => {
  console.log(props);
  return (
    <React.Fragment>
      <button onClick={props.onLogout}>Logout</button>
      <button onClick={props.onLogin}>Login</button>
    </React.Fragment>
  );
};

// export default React.memo(login);
```

> Lazy load

```js
const Posts = React.lazy(() => import('./containers/Posts'));

class App extends Component {
  state = { showPosts: false };

  modeHandler = () => {
    this.setState(prevState => {
      return { showPosts: !prevState.showPosts };
    });
  };

  render() {
    return (
      <React.Fragment>
        <button onClick={this.modeHandler}>Toggle Mode</button>
        {this.state.showPosts ? (
          <Suspense fallback={<div>Loading...</div>}>
            <Posts />
          </Suspense>
        ) : (
          <User />
        )}
      </React.Fragment>
      // <BrowserRouter>
      //   <React.Fragment>
      //     <nav>
      //       <NavLink to="/user">User Page</NavLink> |&nbsp;
      //       <NavLink to="/posts">Posts Page</NavLink>
      //     </nav>
      //     <Route path="/" component={Welcome} exact />
      //     <Route path="/user" component={User} />
      //     <Route
      //       path="/posts"
      //       render={() => (
      //         <Suspense fallback={<div>Loading...</div>}>
      //           <Posts />
      //         </Suspense>
      //       )}
      //     />
      //   </React.Fragment>
      // </BrowserRouter>
    );
  }
}
```

### [Heres how React's New Context API Works](https://www.youtube.com/watch?v=XLJN4JfniH4)
Source code:
- https://github.com/wesbos/React-Context/tree/master/src

### [Building The New Facebook With React and Relay | Ashley Watkins](https://www.youtube.com/watch?v=KT3XKDBZW7M)
How facebook works with react and preloading, they are using Graphql with Relay
they are using `React.Suspense` as a loading container, while the content is loading we can show a loading container:
```html
<React.Suspense
fallback={<MyPlaceHolder/>}
>
    <Post>
        <Header></Header>
        <Content></Content>
    </Post>
</React.Suspense>
```

### [React and the Music Industry | Jameyel "J. Dash" Johnson"](https://www.youtube.com/watch?v=DVQTGidS1yk)

### [Using Hooks and Codegen | Tejas Kumar](https://www.youtube.com/watch?v=cdsnzfJUqm0)

### [Data Fetching With Suspense In Relay | Joe Savona](https://www.youtube.com/watch?v=Tl0S7QkxFE4)

### [Building a Custom React Renderer | Sophie Alpert](https://www.youtube.com/watch?v=CGpMlWVcHok)
Simple implementation of React dom, a interesting fact is that the main library has less then 100 lines.

### [90% Cleaner React With Hooks - Ryan Florence - React Conf 2018](https://www.youtube.com/watch?v=wXLf18DsV-I)
He is showing how to use HOCs to make the code simpler and the reducer HOC
```javascript
const initialState = {count: 0};

function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {count: state.count + 1};
    case 'decrement':
      return {count: state.count - 1};
    default:
      throw new Error();
  }
}

function Counter() {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <>
      Count: {state.count}
      <button onClick={() => dispatch({type: 'decrement'})}>-</button>
      <button onClick={() => dispatch({type: 'increment'})}>+</button>
    </>
  );
}
```

## Angular videos

### [These ARE the Angular tips you are looking for | John Papa](https://www.youtube.com/watch?v=2ZFgcTOcnUg)

multiple unsubscribes with SbSink, but as a dependencie....
```javascript
subs = new SbSink()
subs.add(observable)
subs.add(observable)
subs.unsubscribe()
```

and more about the JAMstack

### [NG Conf 2019 Day 3 CDK Is The Coolest Thing You Are Not Using With Jeremy Elbourn](https://www.youtube.com/watch?v=4EXQKP-Sihw)

### [Tools for Fast Angular Applications | Minko Gechev](https://www.youtube.com/watch?v=5VlBaaXO6ok)

### [Mastering the Subject: Communication Options in RxJS | Dan Wahlin](https://www.youtube.com/watch?v=_q-HL9YX_pk)

---
id: bulma-css
title: Bulma CSS
sidebar_label: Bulma CSS 
---

Its a CSS framework 


### Debugging
````css
/*! debug.css | MIT License | zaydek.github.com/debug.css */
*:not(path):not(g) {
	color:                    hsla(210, 100%, 100%, 0.9) !important;
	background:               hsla(210, 100%,  50%, 0.5) !important;
	outline:    solid 0.25rem hsla(210, 100%, 100%, 0.5) !important;
	box-shadow: none !important;
}
````

### Sizing
as we have titles, we can change their sizer by: `is-size-x` like follow:
```html
<h1 class="title is-size-4">Bulma?</h1>
```

### Collumns
`is-multiline `

`is-0` removes the gap over a grid

`is-mobile` keeps the size over mobile

```html
<div class="columns is-variable is-2">
    <div class="column">
        <figure class="image card">
            <img src="images/blog-1.png">
        </figure>
    </div>
    <div class="column">
        <figure class="image card">
            <img src="images/blog-2.png">
        </figure>
    </div>
    <div class="column">
        <figure class="image card">
            <img src="images/blog-3.png">
        </figure>
    </div>
</div>
```

to center we can use a [level](https://bulma.io/documentation/layout/level/) with one element
```html
<figcaption class="level">
    <small class="level-item">
        The blog we'll build. Click to continue on Scrimba
    </small>
</figcaption>
```

### Images
you can specify a size, so the browser knows how to preload them.
```html
<figure class="image is-128x128">
    <img src="https://bulma.io/images/placeholders/128x128.png">
</figure>
```

### Media
```html
<article class="media center">
    <figure class="media-left">
        <span class="image is-64x64">
            <img src="images/me.jpg">
        </span>
    </figure>
    <div class="media-content">
        <div class="content">
            <p>
                <strong>Zaydek Michels-Gualtieri</strong> <a>@username_zaydek</a><br>
                <span class="has-text-grey">Self-taught, inspired to learn<br>
                <time datetime="2018-04-20">Apr 20</time> · 20 min read</span>
            </p>
        </div>
    </div>
</article>
```

### Helpers
to remove margins:
`is-marginless` 

to remove paddings:
`is-paddingless`



### Helper CSS, not related to bulma
```css
.center, .center-column, .top, .right, .bottom, .left {
	display: flex;
	justify-content: center;
	align-items: center;
}

.center-column {
    flex-direction: column;
}

.top    { align-items:     flex-start; }
.right  { justify-content: flex-end;   }
.bottom { align-items:     flex-end;   }
.left   { justify-content: flex-start; }

.single-spaced, .single-spaced * {
    line-height: 1;
}

@media (max-width: 1024px) {
	.desktop {
		display: none;
	}
}
```

### Bulma docs

```html
<!--
	hero:       https://bulma.io/documentation/layout/hero/
	section:    https://bulma.io/documentation/layout/section/
	image:      https://bulma.io/documentation/elements/image/
	columns:    https://bulma.io/documentation/columns/basics/
	media:      https://bulma.io/documentation/layout/media-object/
	icon:       https://bulma.io/documentation/elements/icon/
	breadcrumb: https://bulma.io/documentation/components/breadcrumb/
	level:      https://bulma.io/documentation/layout/level/
-->

<!-- hero: https://bulma.io/documentation/layout/hero/ -->
<section class="hero">
	<div class="hero-body">
		<!-- ... -->
	</div>
</section>

<!-- section: https://bulma.io/documentation/layout/section/ -->
<section class="section">
	<div class="container">
		<!-- ... -->
	</div>
</section>

<!-- image: https://bulma.io/documentation/elements/image/ -->
<figure class="image is-128x128">
	<img src="https://bulma.io/images/placeholders/128x128.png">
</figure>

<!-- columns: https://bulma.io/documentation/columns/basics/ -->
<div class="columns">
	<div class="column">
		<!-- ... -->
	</div>
</div>

<!-- media: https://bulma.io/documentation/layout/media-object/ -->
<article class="media">
	<figure class="media-left">
		<!-- ... -->
	</figure>
	<div class="media-content">
		<div class="content">
			<!-- ... -->
		</div>
	</div>
</article>

<!-- icon: https://bulma.io/documentation/elements/icon/ -->
<span class="icon has-text-success">
	<i class="fas fa-check-square"></i>
</span>

<!-- breadcrumb: https://bulma.io/documentation/components/breadcrumb/ -->
<nav class="breadcrumb" aria-label="breadcrumbs">
	<ul>
		<li><a href="#"><!-- ... --></a></li>
		<li class="is-active"><a href="#" aria-current="page"><!-- ... --></a></li>
	</ul>
</nav>

<!-- level: https://bulma.io/documentation/layout/level/ -->
<div class="level">
	<div class="level-item">
		<!-- ... -->
	</div>
</div>
```

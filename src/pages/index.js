import React from 'react';
import Layout from '@theme/Layout';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';


function Home() {
    const context = useDocusaurusContext();
    console.log(context);
    const { siteConfig = {} } = context;

    return (
        <Layout
            title={`Hello from ${siteConfig.title}`}
            description="Description will go into a meta tag in <head />">
            <main>
                <div className="home-screen">
                    <SplashContainer/>
                    <AboutContainer/>
                </div>
            </main>
        </Layout>
    );
}


const SplashContainer = () =>
    <div className="homeContainer">
        <div className="homeSplashFade">
            <div id="backdrop">
                <h3>
                    Hello Internet!
                </h3>
            </div>
        </div>
    </div>;


const AboutContainer = function () {
    return (
        <div className="about-container">
            <div className="about-group">
                <img src="/img/caina.jpg" alt="douglas caina picture"/>
                <div>
                    I'm a full-stack software engineer. Here I like to <br/><a href="https://caina.github.io/blog"
                                                                               className="hovered">talk about Software
                    development</a> and share the new stuff I'm learning, mostly if it’s related to functional
                    programming, GOlang or <a
                    href="https://caina.github.io/blog/tags/front-end" className="hovered">front-end</a>.
                    <br/>
                    <br/>
                    If you want to talk, you can reach me over: <a href="https://www.linkedin.com/in/douglascaina/"
                                                                   target="_blank" className="hovered">LinkedIn</a>
                </div>
            </div>
        </div>
    )
};

export default Home;

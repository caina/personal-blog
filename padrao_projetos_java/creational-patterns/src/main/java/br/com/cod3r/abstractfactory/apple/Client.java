package br.com.cod3r.abstractfactory.apple;

import br.com.cod3r.abstractfactory.apple.factory.IPhone11Factory;
import br.com.cod3r.abstractfactory.apple.factory.IPhoneXFactory;
import br.com.cod3r.abstractfactory.apple.factory.abstractFactory.BrazilianRules;

public class Client {

    public static void main(String[] args) {
		BrazilianRules rules = new BrazilianRules();
		new IPhone11Factory(rules).orderIPhone("standard");
		new IPhoneXFactory(rules).orderIPhone("standard");


    }
}

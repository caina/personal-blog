package br.com.cod3r.factory.apple;

import br.com.cod3r.factory.apple.factory.IphoneXFactory;

public class Client {
	
	public static void main(String[] args) {
		IphoneXFactory factory = new IphoneXFactory();
		factory.orderIphone("barato");
		factory.orderIphone("caro");
		factory.orderIphone("bom e barato");
	}
}

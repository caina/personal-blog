package br.com.cod3r.factory.apple.factory;

import br.com.cod3r.factory.apple.model.IPhone;
import br.com.cod3r.factory.apple.model.IPhoneX;
import br.com.cod3r.factory.apple.model.IPhoneXSMax;

public class IphoneXFactory extends IPhoneFactory {
    @Override
    protected IPhone createIphone(String level) {
        if (level.equals("barato")) {
            return new IPhoneX();
        } else if(level.equals("caro")) {
            return new IPhoneXSMax();
        }
        throw new RuntimeException("there is no "+level+ " iphone");
    }
}

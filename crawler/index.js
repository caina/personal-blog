const osmosis = require('osmosis');
const algoliasearch = require('algoliasearch');

//https://steemit.com/utopian-io/@beggars/how-to-scrape-websites-and-api-s-in-node-js-using-osmosis
const client = algoliasearch('ZGVYUM4QFK', '138fee396a110339f2d34755690e3cae');
const angoliaCli = client.initIndex('douglas');

const baseUrl = 'https://caina.github.io';
const notes = `${baseUrl}/docs/a-list-of-usefull-links/`;
const blog = `${baseUrl}/blog`;
const anchors = [];

osmosis
    .get(notes)
    .find('.menu a')
    .set('lvl1_title')
    .set({ 'lvl1_link': '@href' })
    .data(function (listing) {

        if(listing.lvl1_link === "#!") {
            return;
        }

        anchors.push(
            {
                'anchor': listing.lvl1_title,
                'content': null,
                'hierarchy': {
                    'lvl0': 'Notes',
                    'lvl1': listing.lvl1_title,
                    'lvl2': null,
                    'lvl3': null,
                    'lvl4': null,
                    'lvl5': null,
                    'lvl6': null
                },
                'url': `${baseUrl}${listing.lvl1_link}`,
                'objectID': Math.random() * 10000,
            }
        )

    })
    .follow('@href')
    .find('ul.contents a')
    .set('lvl2_title')
    .set({ 'lvl2_link': '@href' })
    .data(function (listing) {
        // console.log(listing)
        if(listing.lvl1_link === "#!") {
            return;
        }
        listing.lvl2_link = listing.lvl1_link + listing.lvl2_link;
        // console.log(listing.lvl1_title, ':', listing.lvl2_title, ':', `${baseUrl}${listing.lvl2_link}`);
        anchors.push(
            {
                'anchor': listing.lvl2_title,
                'content': null,
                'hierarchy': {
                    'lvl0': 'Notes',
                    'lvl1': listing.lvl1_title,
                    'lvl2': listing.lvl2_title,
                    'lvl3': null,
                    'lvl4': null,
                    'lvl5': null,
                    'lvl6': null
                },
                'url': `${baseUrl}${listing.lvl2_link}`,
                'objectID': Math.random() * 10000,
            }
        )
    })
    // .log(console.log)
    // .error(console.log)
    // .debug(console.log)
    .done(() => getLinksFromBlog());
// .done(() => console.log(anchors));

function getLinksFromBlog() {
    osmosis
        .get(blog)
        .find('.container h2 a')
        .set('lvl1_title')
        .set({ 'lvl1_link': '@href' })
        .data(function (listing) {
            // console.log(listing);
            anchors.push(
                {
                    'anchor': listing.lvl1_title,
                    'content': null,
                    'hierarchy': {
                        'lvl0': 'Blog',
                        'lvl1': listing.lvl1_title,
                        'lvl2': null,
                        'lvl3': null,
                        'lvl4': null,
                        'lvl5': null,
                        'lvl6': null
                    },
                    'url': `${baseUrl}${listing.lvl1_link}`,
                    'objectID': Math.random() * 10000,
                }
            )
        })
        // .log(console.log)
        .done(() => getBlogTags());
}

function getBlogTags() {
    osmosis
        .get(`${blog}/tags`)
        .find('.main-wrapper a')
        .set('lvl1_title')
        .set({ 'lvl1_link': '@href' })
        .data(function (listing) {
            // console.log(listing);
            anchors.push(
                {
                    'anchor': listing.lvl1_title,
                    'content': null,
                    'hierarchy': {
                        'lvl0': 'Blog',
                        'lvl1': 'Tags',
                        'lvl2': listing.lvl1_title,
                        'lvl3': null,
                        'lvl4': null,
                        'lvl5': null,
                        'lvl6': null
                    },
                    'url': `${baseUrl}${listing.lvl1_link}`,
                    'objectID': Math.random() * 10000,
                }
            )
        })
        .done(()=> {
            // console.log(anchors)
            angoliaCli.clearIndex(() => {
                angoliaCli.addObjects(anchors, (err, content) => {
                    console.log(err, content);
                });
            })
        })
}

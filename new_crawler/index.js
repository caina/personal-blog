// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');


function crawl(basePath) {
  const metadata = [];

  getFiles(basePath).forEach((file) => {
    const id = getFileId(file);
    const title = getTitle(file);
    const slug = getSlug(file);

    const removeHeader = (text) => text.replace(fileHeaderRegex, '');
    const fileContent = removeHeader(file.content).split(headlineRegex);

    fileContent.forEach((content) => {
      const headline = extractHeadlineFromFirstLine(content);
      const url = () => slug ?
        `docs${slug}` :
        `${removeFileExtention(file.path)}/${id}#${formatAnchorToUrl(headline)}`;

      metadata.push(
        {
          url: url(),
          breadcrumb: `${file.folder ? file.folder + ' / ' : ''}${headline || title}`,
          content: content.replace(specialCharactersRegex, ''),
        },
      );
    });
  });

  return metadata;
}

function extractHeadlineFromFirstLine(text) {
  return text.split('\n')[ 0 ];
}

function removeFileExtention(name) {
  return name.replace(fileExtensionRegex, '');
}

function getFileId(file) {
  return file.content.match(extractIdRegex).join('').split('id: ')[ 1 ];
}

function getTitle(file) {
  return file.content.match(extractTitleRegex).join('').split('title: ')[ 1 ];
}

function getSlug(file) {
  const slug = file.content.match(extractSlugRegex);
  if (!slug) {
    return undefined
  }

  return slug.join('').split('slug: ')[ 1 ];
}

function formatAnchorToUrl(title) {
  return title.toLowerCase().replace(alphabeticalCharactersRegex, '').replace(onlySpacesRegex, '-');
}

const getFiles = (basePath) => fs.readdirSync(basePath, { withFileTypes: true })
  .flatMap((document) => {
    if (document.isDirectory()) {
      return getFiles(`${basePath}/${document.name}`);
    }

    const content = fs.readFileSync(`${basePath}/${document.name}`, 'utf-8');
    const relativePath = path.relative(process.cwd(), basePath);
    const folder = relativePath.split('/')[ 1 ]
    return { path: relativePath, folder, content };
  });

const headlineRegex = /[#?]\s+/mg;
const fileHeaderRegex = /^---.*?---/gs;
const extractIdRegex = /^id:\s(.*)/mg;
const extractTitleRegex = /^title:\s(.*)/mg;
const extractSlugRegex = /^slug:\s(.*)/mg;
const fileExtensionRegex = /\.md$|\.mdx$/;
const alphabeticalCharactersRegex = /[^\w+ ]/g;
const onlySpacesRegex = /\s+/g;
const specialCharactersRegex = /[^\w+ ]/gm;


module.exports = crawl;

// eslint-disable-next-line @typescript-eslint/no-var-requires
const crawl = require('./index');

describe('Crawler list', () => {
  it('should get all the metadata inside files', () => {
    const documentation = crawl(`${__dirname}/docs-test`);
    expect(documentation).toHaveLength(12);
  });

  it('should convert to a searchable object', () => {
    const documentation = crawl(`${__dirname}/docs-test/consent-test/01-consent-test`);

    expect(documentation[ 0 ].url).toEqual('docs-test/consent-test/01-consent-test/this-is-a-test#');
    expect(documentation[ 0 ].content).toContain('From a user perspective and from a legal point of view');
  });

  it('should have a well formatted url', () => {
    const documentation = crawl(`${__dirname}/docs-test`);

    const document = documentation.find(doc => doc.content.includes("Aliquam nec tellus"))

    expect(document.url).toEqual('docs-test/consent-test/01-consent-test/this-is-a-test#aliquam-nec-tellus-vestibulumtest-rhoncus-123-lectus-euismod-porttitor-mi');
  });

  it('should build the breadcrumb without headline', function () {
    const documentation = crawl(`${__dirname}/docs-test`);
    expect(documentation[ 1 ].breadcrumb).toEqual('consent-test / testing')
  });

  it('should get the breadcrumb', () => {
    const documentation = crawl(`${__dirname}/docs-test`);
    expect(documentation[ 3 ].breadcrumb).toEqual('consent-test / Small example')
  });

  it('should not contain special characters', function () {
    const documentation = crawl(`${__dirname}/docs-test`);

    const content = documentation.map(doc => doc.content).join('')

    expect(content).not.toContain('#')
  });

  it('should use slug if present', function () {
    const documentation = crawl(`${__dirname}/docs-test`);
    const document = documentation.find(doc => doc.breadcrumb.includes("what is lorem ipsun?"));

    expect(document.url).toBe("docs/lorem");
  });

  it('should crawl docs', () => {
    const documentation = crawl(`${__dirname}/../docs`);
    expect(documentation).toBeTruthy();
  });
});

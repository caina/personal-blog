/*
  This file creates the file searchable.json out of our documentation.
  We are using this file to do in browser search for documents.
 */
const fs = require('fs');
const crawl = require('./new_crawler');

const docs = crawl(`${__dirname}/docs`)

fs.writeFileSync('static/searchable.json', JSON.stringify(docs));

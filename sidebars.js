/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
    someSidebar: {
        'Videos About Programming': [
            'videos-about-agile',
            'videos-about-software-development',
            'videos-about-architecture',
            'videos-about-front-end',
            'videos-about-go-lang'
        ],
        'Links': [
            'a-list-of-usefull-links',
            'the-open-source-computer-science-degree'
        ],
        'Code Snippets': [
            'bash-shell',
            'kotlin-coursera-class',
            'regex-cod3rs-class',
            'angular-advanced-snippets',
            'flutter-usefull-resources',
            'bulma-css',
        ],
        'Abstract': [
            'german-learne',
            'design-pattern',
            'hooked-how-to-create-habit-forming-apps'
        ],

    },
};

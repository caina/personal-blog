---
id: the-weight-of-a-dependency-in-a-modernd-spa-application
title: The weight of a dependency in a modern SPA application
tags: [front-end, SPA] 
---
![node modules joke](/blog/node_modules.jpeg)

These are odd days, back on the days, if you want to do an API call, you'd have to build a client, but now, everything can be solved by a single npm install.
I'm not complaining, I really like how it saved time and made our job simpler, but I'd like to point out some problems and why we should be more careful while installing someone else code into our software.

<!--truncate-->

### The joy of building:
We definitely don’t work anymore in cool stuff, at any new requested feature, the first step is to rush to google searching for a way out. Where is the joy of building animations or [popovers](https://medium.com/@douglascaina/lets-create-an-interactive-popover-in-angular-5-from-scratch-99bff3b5153c)? Even its already developed by someone, its always nice to know how it works.

### The node_modules folder is so big that it has its own gravity.
The node_modules folder became so heavy that it seems like it has its own gravity, every time you have to do a clean install you can take a break for a coffee. Pipelines are now slower, bundles now are huge, thus taking more time to load our applications, so much that we now need to use "micro front-ends" to handle the load into chunks (I know that's not the only reason, but it is one though). 

### We expose our applications
If a library got hacked, they can easily be used to inject a crypto-miner into our applications. That is such a bit problem, that we have [Subresource integrity check in browser](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity) to not have surprises.

### Sharing dependencies
Let’s suppose that we are using a quite popular library called: puppy.js@1.3 and the desired library has the same as a dependency but in version 0.4, not only your bundle now will have 2 versions of puppy.js but if the library doesn’t keep updating it, you'll have a security issue out of hands.

I know that libraries are easier and really helpful, even more nowadays that we don’t have time to build an app as we would like to. I'm not saying to not use them, instead, think if it’s really necessary, check the code and its dependencies and what value this code will add to you. 

Personally, if I find a small library interesting, I tend to copy its code, revise it and place in my codebase, if I find a bug, I can easily fix or even open a pull request for the maintainer (even though I'm not using it). Not only the above problems are hard to keep track but cross dependencies can be really hard to update and maintain a long term application is no easy job in these times of npm install.


---
id: angular-simple-feature-toggle-that-works-outside-of-the-route
title: Creating a featureToggle directive in Angular.
tags: [angular, front-end]
---

![Photo by Anthony Intraversato on Unsplash](/blog/airplane.jpg)

This is kind of odd, but my PO wanted to have a feature toggle so our users would not see the new stuff but we want to test this thing against the real world. So let's build it because we libraries are way too dangerous these days 👻.

<!--truncate-->

> Our feature toggle is going to rely on the browser query parameters to work, so in order to enable a feature, we'll have to navigate with the following URL: `localhost:4200?feature=search-bar`

To use our feature toggle, we are going to create a directive, that we can easily attach to the component we want to toggle: `<app-search-bar *featureToggle="'search-bar'" />`.

the *first* thing that we need is to store every path parameter as features so we can toggle the hell out of our app. for this we are going to create a resolver:
```angular2
@Injectable({providedIn: 'root'})
export class FeatureResolver implements Resolve<void> {
  constructor(private _featureService: FeatureService) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): void {
    const feature = route.queryParamMap.get('feature');
    if (feature != null) {
      this._featureService.addAvailableFeature(feature);
    }
  }
}
```
and this resolver is going to be used in our route as the route resolver:
```javascript
const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    resolve:[
      FeatureResolver // <- thats the thing
    ]
  }
];
```

the idea here is that we can enable these features by route! Every time we enter the route we'll take the path parameters and store them in service. I mean, a singleton service! Be aware of it because if you provide it in the wrong place you'll not get the values from the route.

and this service is looking something like this:
```angular2
@Injectable()
export class FeatureService {

  private featuresSource = new BehaviorSubject<Array<String>>([]);
  public $features = this.featuresSource.asObservable();

  public addAvailableFeature(feature: string): void {
    this.featuresSource.next([...this.featuresSource.value, feature])
  }
}
```
In case you've never seen something like this pattern, this is an observable pattern, maybe we can talk about it later, but the advantage of this thing is that NO ONE TOUCH IN MY OBSERVABLE 👺. You can read but not send data. 

> Why the hell do we need this? as we are relying on the route to getter the path parameter, if the component you want to test is outside, like a header or a footer, it would not work, so we handle it using promises.

and to finish, we just need the directive, and it's as simple as:
```angular2
@Directive({selector: '[featureToggle]'})
export class FeatureToggleDirective implements OnInit {

  @Input('featureToggle') feature: string;
  
  constructor(private featureService: FeatureService,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  ngOnInit(): void {
    this.featureService.$features.subscribe(features => {
      if(features.includes(this.feature)) {
        this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
          this.viewContainer.clear();
        }
    })
  }
}
```

The problem that I had is that the feature to be toggled is in the header, the header is outside of the route so the directive is resolved first, and the route would only collect the "features to be toggled" when angular finish to inject the route, to be worst, my route is being lazy loaded, so it takes even more time. This subscription actually solved a lot of problems.


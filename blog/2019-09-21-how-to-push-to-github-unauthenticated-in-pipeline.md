---
id: how-to-push-to-github-unauthenticated-in-pipeline
title: Pushin to github in a pipeline without loging.
tags: [github, gitlab, cicd, pipeline]
---
If you want to push to github, (github pages as an example) the easier way is using the following code:

```bash
git push https://{username}:{password}@github.com/username/repo.git --all
```

if you push like this, the url that you're providing act as the `remote` of the `git push origin --all`.
I ended up having to do this because this website is being hosted on github pages, but the source code is in gitlab and the pipeline is the gitlab pipeline. 

so to achieve this, the `.gitlab-cy.yml` is like this:
```yaml
publish on github pages:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  cache:
    policy: pull
    key: site-package
    paths:
      - ./website/build
  script:
    - cd ./website/build/douglas-caina
    - git config --global user.email ${GITHUB_EMAIL}
    - git config --global user.name ${GITHUB_USER}
    - git init
    - git add .
    - git commit -m "new version"
    - git push https://${GITHUB_USER}:${GITHUB_PASSWORD}@github.com/${GITHUB_USER}/${GITHUB_REPO} --all -f
  environment:
    name: production
    url: https://caina.github.com
```

I have an build phase before, so thats why my git strategy is none.

---
id: angular-testing-without-before-each-for-a-better-code
title: Angular TDD without BeforeEach, and why its a good idea.
tags: [angular, front-end, TDD] 
---


Its been a while since I started to do some TDD with Angular and one of the things that start to bother me was the beforeEach mutations, you never know what is on the fixture, you never get if the component is being created on the beforeEach with the async, or the one without.

<!--truncate-->
 
One way to solve this is to create a "setup" function, some simple like:
```javascript
describe('my test', function(){
    function setUp() {
        const routes = [
            {path: 'test', component: MyComponent},
        ] as Routes;

        TestBed.configureTestingModule({
          declarations: [MyComponent],
          imports: [
            TranslateModule.forRoot(), 
            RouterTestingModule.withRoutes(routes)
          ],
          providers: [TranslateService]
        }).compileComponents();
        
        const router: Router = TestBed.get(Router);
        const fixture = TestBed.createComponent(MyComponent);
        
        return {
          fixture,
          router
        };
    }
    
    it('should test', () => {
        const {fixture} = setUp();
        fixture.detectChanges();
    }); 

});
```

the Idea is to every test create the testbed and return it as an object, meanwhile the test that runs it would destruct the variables. No mutations, everyone is a const and you are now in charge of the code.

If you step into a problem that you have more then one scenario to cover, you can create multiple setups and they would never step in each other's toes, something that the beforeEach could not give you.

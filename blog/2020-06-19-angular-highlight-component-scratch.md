---
id: angular-highlight-text-component-without-dependencies
title: Simple Angular search highlighting text without dependencies
tags: [angular, front-end, SPA] 
---

![highlight-text-angular](/blog/book-highlight-text.jpg)

These days we had a feature to implement, a search text highlight component, it's like when you press "control + f" on your browser and it highlight the content so you can spot it.

The idea here is to build a simple, but functional text highlight component that is going . Much like a marker on a book although without any external dependency, since we know that a `npm install` [can overwhelm our application](https://caina.github.io/blog/the-weight-of-a-dependency-in-a-modernd-spa-application).


<!--truncate-->
 

The search criteria can have spaces and is able to highlight multiple occurrences, so if you search for `the house`, the words "the" and "house" can be separated in the content.

just create a `highlight.component.ts` and paste the following code.
```javascript
import {Component, Input, OnInit} from '@angular/core';
import {SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'app-highlight',
  template: `<span [innerHTML]="contentHtml"></span>`
})
export class HighlightComponent implements OnInit {

  @Input()
  content: string;

  @Input()
  searchTerm: string;

  contentHtml: SafeHtml;

  ngOnInit(): void {
    if (!this.searchTerm) {
      this.contentHtml = this.content;
    } else {

      const sanitizedSearchTerm = (searchTerm) => searchTerm.split(' ').filter(it => !!it).join('|');

      this.contentHtml = this.content.replace(new RegExp(sanitizedSearchTerm(this.searchTerm), 'gi'), match => {
        return `<span class="highlight">${match}</span>`;
      });
    }
  }
}
```

> !Important, the css for `.highlight` CANNOT be in the same folder, in this case, the css must be place in the `.styles.scss` from the root folder.

Just some explanations about the code:
```javascript
(searchTerm) => searchTerm.split(' ').filter(it => !!it).join('|');
```
imagine that we receive the input: 
- "a  b", `search.split` is going to create the array `['a',' ','b']`
- `.filter(it => !!it)` is going to remove the white space `['a', 'b']`
- `.join("|")` is joining the words appending the regex property "or", creating: "a|b" as result.

this is going to search the entire content and apply the span for not only "a" but "b".

---
id: frontend-angular-react-architecture-scales
title: Front-end for Single page applications(Angular, React, Vue) that scales with your team and how to makes sense of it.
tags: [angular, react, front-end] 
---

![creative-patrck-front-end](/blog/creative-patrick.gif)

In over 3 years of this new SPA world, I've seen a-lot and today, I want to explain how I structure things arround. as an disclosure, I'm not saying that's a silver bullet, you might have to change it to fit your needs.

<!--truncate-->

### Front-end Its all about Data
When I started on this web thing, front-end was just a way to display information, and I still think that's true. Even though the internet those days is impressive fast, any megabyte to open a website may be a problem. Moreover, we have a boom of mobile devices and bad 3g signals are quite normal.

Taking this into consideration, the app must be just a reflection of the information to display. Any kind of business logic must be executed in the backend, where you have more memory and execution power.

### Data manipulation between components at different levels

The biggest problem that I've faced with SPA's is the iteration between components in different locations. Let's say that we have an e-commerce, and every time the user press a like button under the product description, this product goes to a list in the header. You can easily send an API request to the backend with the product id and user, but how would you notify the header that there is a new product to display?

Well most of my previous project, we couple the header and the like button with a function, so when we like the header updates. In angular, you could create a service and share its logic and in React a Redux store could solve it, but then, the header and the product detail would have a relation that would not be that clear.

This can work for some time, but as the application will grow, these components will be so strongly coupled, that you'll end up redoing the same component over and over again, or worst, start to have some really hard bugs to fix.

<!--truncate-->

### Layers
I like to break my applications into 3 layers (That's not a rule but I like to follow it as much as possible). We have the **Logic components**, the **Behaviour components**, and the **Dummy components**, I'm not talking anything new but let’s dive into this.

> The number of components layers is important because if it starts to get to "deep", would be hard to make sense of your code.

| Component | Reusability | What it is use to | 
|-----------| ------------|-------------------|
| **Logic components**| None| Interact with Api's and mount the structure for the Behaviour components.|
| **Behaviour components** | Somehow, but unlike | Group the dumb components, imagine a form that enables a button if all the fields are set, or a password and repeat password interaction, the behaviour component would do this logic.|
| **Dummy components**| All the time | this component must receive everything as a dependency. |

Summing up: The first layer calls the backend to get a list of products, send them to a component to distribute the data across the dumb layer.
### Folder strucure
 So, this is really important, no folder has more than 7 files, more than that and you'll get lost. 

But **HOW** is it possible? 1 module in angular has at least 5 files! Well, bear with me.
![questions-patrick](/blog/bla-bla-bla-patrick.jpg)

Imagine that every **Page** is a flow of your app, some people call it even Scenes, because they describe a scene of your app. So we have then a pages folder to list all this "flows", every flow has a sub-flow, every flow has specific components and so on, check out this scaffold. 

```
+-config.js
+-routes.js
+-app.js
+--i18n/
|  |-index.js
|  |-en.js
|  |-pt_br.js
+--components/
|  |-Button.js
+--pages/
|  +-store.js
|  +-login/
|   |-- login.page.js
|   |-- login.service.js
|   |-- components/
|   |   +- login-form.component.js
|  +-blog/
|  |   +- blog.store.js
|  |   +- blog.service.js
|  |   +- components/
|  |   |  +-- post-avatar.js
|  |   |  +-- comment-button.js
|  |   +- post/
|  |   |  +- post.service.js
|  |   |  +- post.page.js
|  |   +- post-detail/
|  |   |  +- post-detail.page.js
|  |   |  +- post-detail.service.js
|  |   |  +- components/
|  |   |    |-- user-comment.js
|  |   |    |-- hero-banner.js
|  |   |    |-- post-content.js
|  |   +- post-create/
|  |   |  +- post-create.page.js
|  |   |  +- post-create.service.js
```
As the root of our app, we want to configure things and everything related to the bootstrap phase, if you run into multiple files with similar goals, try to create folders here, like the `i18n/`. But usually, here we only configure things.

In the same level, we have our pages, the "flows" that we were talking about. In this case, it's the login and blog sections, they are combining flows that share responsibilities, like the blog section that has posts and everything related to its creation, doing so we can easily share they services and components. 

> Login could be part of a **user** module if you think in the relation of the data between the login phase and user manipulation, although they would hardly share visual components.

Under the **pages/post** we have a chain of flows, they have at a certain level shared data or components, in an Angular application would be easy to share models and services here. Most of the times I end up with this `components` folder under the main flow folder to share components seems weird to have this folder spread across the app but in my head it makes sense, I don't want to span the main components folder with things that are not ready to be shared to all levels of our app.

> IMPORTANT: The components under `pages/blog/post/post-detail/components` don't do any kind of business logic, all of its information comes as properties, and every action is a dependency that comes as a property as well, they will NEVER be connected by redux, they'll never have a service injected in their constructor or any sort of smart interaction. Its easier to test something that we control.

You might be thinking to yourself: "Why the user-comment component under the post folder doesn’t retrieve the comments by his own?". Well, imagine that you want to have the number of total comments in the hero-banner component, as they are siblings, you would have to download this information under the comments section, send it back to `post-detail.page.js` and then send it back to `hero-banner.js`. This would create a tight coupling between both components, they'll not be a unit by themself, they will be hard to test and when one of this small components break, it would end up breaking its entire environment.

So this is the architecture that I use all the time, It works pretty well in most of the cases, thanks for reading 😗

![tranks for reading patrick](/blog/patrick-goodbye.gif)

[Slides of the talk](https://docs.google.com/presentation/d/1iDQN3p5Upzv8t_q6sT-K8pF9Kgt5qeb7YjC9uOsV8_c/edit?usp=sharing)
